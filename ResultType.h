//
// Created by dragoon on 11/12/17.
//

#ifndef CPP_CGDK_RESULTTYPE_H
#define CPP_CGDK_RESULTTYPE_H

#endif //CPP_CGDK_RESULTTYPE_H

enum ResultType {
    WAIT, SKIP, DONE, ACTION_SET
};