//
// Created by dragoon on 11/9/17.
//

#include "Action.h"
#include "MyStrategy.h"

int Action::idCounter;

Action::Action() {
    this->waitCooldown = true;
    this->id = idCounter++;
}

Action::Action(bool waitCooldown) : waitCooldown(waitCooldown) { this->id = idCounter++; }

Action::Action(bool waitCooldown, bool singleSignal) : waitCooldown(waitCooldown), singleSignal(singleSignal) {}

ActionResult Action::doSmth(MyStrategy &myStrategy) {
    if (waitCooldown && VarStorage::currentPlayer->getRemainingActionCooldownTicks()) {
        return WAIT;
    }
    ActionResult result = doAction(myStrategy);
    if (result.isDone() && !signalTo.empty()) {//activate all signals;
        auto iterator = myStrategy.actions.begin();
        ++iterator;
        for (auto &action : signalTo) {
            if (action->signalFrom.size() == 1 || action->singleSignal) {
                myStrategy.addActionAfter(iterator, action);
            } else {
                //check if ready
                bool found = true;
                for (auto &signalFromAction : action->signalFrom) {
                    if (signalFromAction == this) {
                        continue;
                    }
                    found = false;
                    for (int &value : myStrategy.sgn) {
                        if (value == signalFromAction->id) {
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        break;
                    }
                }
                if (found) {
                    for (auto signalFromAction : action->signalFrom) {
                        if (signalFromAction == this) {
                            continue;
                        }
                        for (int idx = 0; idx != myStrategy.sgn.size(); ++idx) {
                            if (myStrategy.sgn[idx] == signalFromAction->id) {
                                myStrategy.sgn[idx] = *myStrategy.sgn.rbegin();
                                myStrategy.sgn.pop_back();
                                break;
                            }
                        }
                    }
                    myStrategy.addActionAfter(iterator, action);
                } else {
                    myStrategy.sgn.push_back(this->id);
                }
            }
        }
    }
    return result;
}