//
// Created by dragoon on 11/11/17.
//

#ifndef CPP_CGDK_SIGNALIZERACTION_H
#define CPP_CGDK_SIGNALIZERACTION_H


#include "SignalAction.h"

struct SignalizerAction : public Action {
    std::vector<SignalAction *> signalTo;

    SignalizerAction(const std::vector<SignalAction *> &signalTo) : signalTo(signalTo) {
        for (size_t i = 0; i != this->signalTo.size(); ++i) {
            ++this->signalTo[i]->signals;
        }
    }

    int doSmth(MyStrategy &myStrategy) {
        for (size_t i = 0; i != this->signalTo.size(); ++i) {
            --this->signalTo[i]->signals;
        }
        return -1;
    }
};


#endif //CPP_CGDK_SIGNALIZERACTION_H
