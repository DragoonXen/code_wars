//
// Created by dragoon on 11/9/17.
//

#ifndef CPP_CGDK_STATICVARIABLESSTORAGE_H
#define CPP_CGDK_STATICVARIABLESSTORAGE_H


#include "model/Player.h"
#include "model/Move.h"
#include "model/World.h"
#include "MyVehicle.h"
#include "Action.h"
#include "Constants.h"
#include "GroupBlocks.h"
#include "MoveDirection.h"
#include <list>
#include <unordered_set>

struct Action;

class VarStorage {
public:
    static const model::World *currentWorld;
    static const model::Player *currentPlayer;
    static const model::Player *opponentPlayer;
    static model::Move *currentMove;

    static std::vector<GroupBlocks> groupBlocks;

    static double jetInfluenceField[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize];
    static double heliInfluenceField[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize];
    static double tankInfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];
    static double IFVinfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];
    static double ARRVInfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];
    static double influenceFieldTemp[10][Constants::influenceMapSize][Constants::influenceMapSize];

    static std::vector<MyVehicle *> selectedUnits;

    static std::vector<MyVehicle *> myVehiclesVc;
    static std::vector<MyVehicle *> enemyVehiclesVc;

    static std::vector<MyVehicle *> myVehiclesByType[];
    static std::vector<MyVehicle *> enemyVehiclesByType[];

};


#endif //CPP_CGDK_STATICVARIABLESSTORAGE_H
