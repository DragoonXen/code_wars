//
// Created by dragoon on 08.11.17.
//

#ifndef CPP_CGDK_CALCUTIL_H
#define CPP_CGDK_CALCUTIL_H

#include <unordered_map>
#include "model/Game.h"
#include "Point.h"
#include "MyVehicle.h"
#include "MyStrategy.h"
#include "ExpectedMoveResult.h"

class CalcUtil {
public:

    static model::Game game;

    static model::VehicleType startingPosition[3][3];

    static double startCoords[];

    static double maxSpeed[];

    static double visionRange[];

    static bool isGroundUnit[];

    static int damageTo[int(model::VehicleType::_COUNT_)][int(model::VehicleType::_COUNT_)];

    static double squadsDistance;

    static std::array<model::VehicleType, 5> vehicleTypes;

    static std::vector<MyVehicle *> const &getSelectedUnits();

    static void getUnitsOfType(std::vector<MyVehicle *> &vector,
                               model::VehicleType vehicleType,
                               std::vector<MyVehicle *> &dest);


    static void semiLinearSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                      double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]);

    static void closeRangeSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                      double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]);

    static void closestRangeSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                        double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]);

    inline static void clearField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize]) {
        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {
                from[i][j] = 0.;
            }
        }
    }

    static std::vector<int> getCounts(const std::unordered_map<long long, MyVehicle> &vehicles);

    static Point getCenterCoords(const std::vector<MyVehicle *> &source);

    static double getMaxDistance(const std::vector<MyVehicle *> &source, const Point &dest);

    inline static bool isAerial(const MyVehicle &vehicle) {
        return vehicle.vType == model::VehicleType::HELICOPTER || vehicle.vType == model::VehicleType::FIGHTER;
    }

    inline static double getMaxSpeed(model::VehicleType vType) {
        return CalcUtil::maxSpeed[int(vType)];
    }

    inline static double getVisionRange(model::VehicleType vType) {
        return CalcUtil::visionRange[int(vType)];
    }

    inline static bool isGround(model::VehicleType vType) {
        return CalcUtil::isGroundUnit[int(vType)];
    }

    static void getStartingCoords(model::VehicleType vehicleType, int &x, int &y);

    static int avaliableActionsCount(const MyStrategy &myStrategy);

    static int visAvaliableActionsCount(const MyStrategy &myStrategy);

    static double getMaxRotationSpeed(const std::vector<MyVehicle *> &source, Point around);

    static bool isGroupSelected(int groupNo);

    static double globalSpreadMatrix[Constants::influenceMapSize * 2 - 1][Constants::influenceMapSize * 2 - 1];

    static double closeRangeSpreadMatrix[Constants::closeRangeSpread][Constants::closeRangeSpread];

    static double closestRangeSpreadMatrix[Constants::closestRangeSpread][Constants::closestRangeSpread];

    inline static double distancePointToSegment(const Point &point, const Point &segA, const Point &segB) {
        return point.distance(nearestSegmentPoint(point, segA, segB));
    }

    inline static double sqrDistancePointToSegment(const Point &point, const Point &segA, const Point &segB) {
        return point.sqrDistance(nearestSegmentPoint(point, segA, segB));
    }

    inline static Point nearestSegmentPoint(const Point &point, const Point &segA, const Point &segB) {
        Point vectorV = segA - segB;
        Point vectorW = point - segB;

        double c1 = vectorV.scalarMult(vectorW);
        if (c1 <= 0)
            return segB;

        double c2 = vectorV.scalarMult(vectorV);
        if (c2 <= c1)
            return segA;

        return segB + (vectorV * (c1 / c2));
    }

    static void initSpreadMatrix();

    inline static double getAngle(int direction) {
        return Constants::PI * 2. / Constants::directionsCount * direction;
    }

    static ExpectedMoveResult checkDirection(int direction,
                                             const std::vector<MyVehicle *> &group,
                                             GroupBlocks &moveBlock,
                                             int maxTicks);

    static ExpectedMoveResult checkWaiting(const std::vector<MyVehicle *> &group,
                                           const GroupBlocks &waitingBlock);


};


#endif //CPP_CGDK_CALCUTIL_H
