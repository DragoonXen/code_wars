//
// Created by dragoon on 11/11/17.
//

#include <unordered_set>
#include "Piatnashki.h"

struct Pos {
    int x, y;

    Pos() {}

    Pos(int x, int y) : x(x), y(y) {}
};

int calcHash(std::vector<Pos> &vc) {
    int rez = 0;
    for (size_t i = 0; i != vc.size(); ++i) {
        rez <<= 3;
        rez |= vc[i].x;
        rez <<= 3;
        rez |= vc[i].y;
    }
    return rez;
}

const int d[9][2] = {{0,  0},
                     {1,  0},
                     {-1, 0},
                     {0,  1},
                     {0,  -1},
                     {1,  1},
                     {-1, -1},
                     {-1, 1},
                     {1,  -1}};
const int sX = 4;
const int sY = 4;

inline int makeNew(const std::vector<Pos> &from, std::vector<int> &move, std::vector<Pos> &dest, std::unordered_set<int> &availableActions) {
    for (size_t i = 0; i != dest.size(); ++i) {
        dest[i].x = from[i].x + d[move[i]][0];
        if (dest[i].x < 0 || dest[i].x >= sX) {
            return -1;
        }
        dest[i].y = from[i].y + d[move[i]][1];
        if (dest[i].y < 0 || dest[i].y >= sY) {
            return -1;
        }
    }
    int hash = calcHash(dest);
    if (availableActions.find(hash) != availableActions.end()) { // already exists
        return -1;
    }
    // here check if such move possible
    for (size_t i = 0; i != dest.size(); ++i) {
        for (size_t j = i + 1; j != dest.size(); ++j) {
            if (dest[i].x == dest[j].x && dest[i].y == dest[j].y) { // can't have same destination
                return -1;
            }
            if (move[i] == 0 || move[j] == 0) {// if at least one not moving - all is ok
                continue;
            }
            if (abs(dest[i].x - dest[j].x) > 1 ||
                abs(dest[i].y - dest[j].y) > 1 ||
                abs(from[i].x - from[j].x) > 1 ||
                abs(from[i].y - from[j].y) > 1) { // wasn't intersect if start or finish far
                continue;
            }
            if (from[i].x == dest[j].x && from[i].y == dest[j].y && from[j].x == dest[i].x && from[j].y == dest[i].y) {
                return -1;
            }

            if (abs(d[move[i]][0] - d[move[j]][0]) + abs(d[move[i]][1] - d[move[j]][1]) <= 1) {// similar direction
                continue;
            }
            if ((move[i] + 1) / 2 != (move[j] + 1) / 2) { // not parallel
                return -1;
            }
        }
    }
    availableActions.insert(hash);
    return hash;
}

inline void generateMoves(size_t size, std::vector<std::vector<int>> &dest) {
    dest.clear();
    std::vector<int> vc(size);
    while (vc[0] == 0) {
        dest.push_back(vc);
        auto iter = vc.rbegin();
        ++*iter;
        while (*iter == 9) {
            *iter = 0;
            ++(*(++iter));
        }
    }
    while (vc[0] < 9) {
        dest.push_back(vc);
        auto iter = vc.rbegin();
        ++*iter;
        while (*iter == 9) {
            if ((iter + 1) == vc.rend()) {
                break;
            }
            *iter = 0;
            ++(*(++iter));
        }
    }
}

std::vector<std::vector<std::pair<int, int>>>
Piatnashki::getPiatnashkiMovements(const std::vector<std::pair<int, int>> &start, const std::vector<std::pair<int, int>> &dest) {
    std::unordered_set<int> availableActions;
    std::vector<std::vector<Pos>> queue;
    std::vector<std::vector<int>> change;
    std::vector<std::vector<int>> generatedMoves;
    std::vector<int> parent;

    std::vector<Pos> current;
    current.reserve(dest.size());
    std::vector<Pos> to;
    to.reserve(dest.size());
    {
        std::vector<int> changeV;

        for (size_t i = 0; i != dest.size(); ++i) {
            current.emplace_back(dest[i].first, dest[i].second);
            to.emplace_back(start[i].first, start[i].second);
            changeV.emplace_back(0);
        }
        queue.push_back(current);
        change.push_back(changeV);
    }

    availableActions.insert(calcHash(current));
    parent.push_back(-1);

    int finish = calcHash(to);

    if (availableActions.find(finish) == availableActions.end()) {
        generateMoves(3, generatedMoves);
        bool found = false;
        for (size_t idx = 0; idx != queue.size(); ++idx) {
            for (size_t i = 0; i != generatedMoves.size(); ++i) {
                int newHash = makeNew(queue[idx], generatedMoves[i], current, availableActions);
                if (newHash != -1) {
                    queue.push_back(current);
                    parent.push_back(idx);
                    change.push_back(generatedMoves[i]);
                    if (newHash == finish) {
                        found = true;
                        break;
                    }
                }
            }
            if (found) {
                break;
            }
        }
    }
    std::vector<std::pair<int, int>> rez;
    std::vector<std::vector<std::pair<int, int>>> finalrez;
    int idx = queue.size() - 1;
    while (idx != -1) {
        for (size_t i = 0; i != queue[idx].size(); ++i) {
            rez.emplace_back(queue[idx][i].x, queue[idx][i].y);
        }
        finalrez.push_back(rez);
        rez.clear();
        idx = parent[idx];
    }
    return finalrez;
};

