//
// Created by dragoon on 11/13/17.
//

#ifndef CPP_CGDK_JETINFLUENCEMAPCALC_H
#define CPP_CGDK_JETINFLUENCEMAPCALC_H

#include <cstring>
#include <chrono>
#include <algorithm>
#include "Action.h"
#include "MyStrategy.h"
#include "CalcUtil.h"
#include "SimpleLog.h"

struct I_JetInfluenceMapCalc {

    static void calculateJetField(MyStrategy &myStrategy) {
        auto start = std::chrono::steady_clock::now();
        double divizion = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
        double (&jetInfluenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::jetInfluenceField[0];

        double (&coverField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::jetInfluenceField[1];
        double (&attackField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::jetInfluenceField[2];
        double (&defenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::jetInfluenceField[3];

        double (&tempLayer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[0];
        double (&layer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[1];
        double (&layer2)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[2];

        CalcUtil::clearField(coverField);
        if (!VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)].empty() &&
            !VarStorage::myVehiclesByType[int(model::VehicleType::HELICOPTER)].empty()) {
            CalcUtil::clearField(tempLayer1);
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
            }
            CalcUtil::semiLinearSpreadField(tempLayer1, layer1);
            //1 - fighter distance

            CalcUtil::clearField(tempLayer1);
            for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
            }
            CalcUtil::semiLinearSpreadField(tempLayer1, layer2);

            std::vector<std::pair<double, std::pair<int, int>>> maxes;
            for (int i = 0; i != Constants::influenceMapSize; ++i) {
                for (int j = 0; j != Constants::influenceMapSize; ++j) {
                    if (maxes.size() < 4) {
                        maxes.emplace_back(layer1[i][j], std::make_pair(i, j));
                        std::sort(maxes.rbegin(), maxes.rend());
                    } else if (maxes[3].first < layer1[i][j]) {
                        int idx = 2;
                        while (idx != -1) {
                            if (maxes[idx].first < layer1[i][j]) {
                                --idx;
                            } else {
                                break;
                            }
                        }
                        ++idx;
                        for (int x = 3; x > idx; --x) {
                            maxes[x] = maxes[x - 1];
                        }
                        maxes[idx] = std::make_pair(layer1[i][j], std::make_pair(i, j));
                    }
                }
            }
            double maxHeliInfluence = 0;
            for (auto &val : maxes) {
                double &dVal = layer2[val.second.first][val.second.second];
                if (dVal > maxHeliInfluence) {
                    maxHeliInfluence = dVal;
                }
            }
            maxHeliInfluence = 1 / maxHeliInfluence;
            maxHeliInfluence -= maxHeliInfluence / 10.;
            maxHeliInfluence = 1 / maxHeliInfluence;

            maxes.clear();
            for (int i = 0; i != Constants::influenceMapSize; ++i) {
                for (int j = 0; j != Constants::influenceMapSize; ++j) {
                    if (maxes.size() < 4) {
                        maxes.emplace_back(layer2[i][j], std::make_pair(i, j));
                        std::sort(maxes.rbegin(), maxes.rend());
                    } else if (maxes[3].first < layer2[i][j]) {
                        int idx = 2;
                        while (idx != -1) {
                            if (maxes[idx].first < layer2[i][j]) {
                                --idx;
                            } else {
                                break;
                            }
                        }
                        ++idx;
                        for (int x = 3; x > idx; --x) {
                            maxes[x] = maxes[x - 1];
                        }
                        maxes[idx] = std::make_pair(layer2[i][j], std::make_pair(i, j));
                    }
                }
            }
            double maxFighterInfluence = 0;
            for (auto &val : maxes) {
                double &dVal = layer1[val.second.first][val.second.second];
                if (dVal > maxFighterInfluence) {
                    maxFighterInfluence = dVal;
                }
            }
//        maxFighterInfluence = 1 / maxFighterInfluence;
//        maxFighterInfluence *= 4. / 3.;
//        maxFighterInfluence = 1 / maxFighterInfluence;

            for (int i = 0; i != Constants::influenceMapSize; ++i) {
                for (int j = 0; j != Constants::influenceMapSize; ++j) {
                    coverField[i][j] = std::min(std::min(layer1[i][j] / maxFighterInfluence, layer2[i][j] / maxHeliInfluence), 1.);
                    coverField[i][j] = std::max(coverField[i][j], .8);
                }
            }
        }

        CalcUtil::clearField(tempLayer1);
        {
            const int damage = CalcUtil::game.getIfvAerialDamage() - CalcUtil::game.getFighterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::IFV)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterAerialDamage() - CalcUtil::game.getFighterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        {
            const int damage = CalcUtil::game.getFighterAerialDamage() - CalcUtil::game.getFighterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        CalcUtil::closestRangeSpreadField(tempLayer1, defenceField);
        CalcUtil::semiLinearSpreadField(tempLayer1, VarStorage::jetInfluenceField[4]);// danger meter

        CalcUtil::clearField(tempLayer1);
        {
            const int damage = CalcUtil::game.getFighterAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        {
            const int damage = CalcUtil::game.getFighterAerialDamage() - CalcUtil::game.getFighterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        CalcUtil::closeRangeSpreadField(tempLayer1, attackField);
        CalcUtil::semiLinearSpreadField(tempLayer1, layer2);

        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {
                jetInfluenceField[i][j] = layer2[i][j] * 1e-7
                                          + 200. * coverField[i][j]
                                          + 1.5 * attackField[i][j]
                                          - .32 * defenceField[i][j];
            }
        }
        auto now = std::chrono::steady_clock::now();
        long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
        LOG_DEBUG("% microseconds", times);
    }
};


#endif //CPP_CGDK_JETINFLUENCEMAPCALC_H
