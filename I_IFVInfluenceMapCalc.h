//
// Created by dragoon on 11/13/17.
//

#ifndef CPP_CGDK_I_IFVINFLUENCEMAPCALC_H
#define CPP_CGDK_I_IFVINFLUENCEMAPCALC_H

#include <cstring>
#include <chrono>
#include <algorithm>
#include "Action.h"
#include "MyStrategy.h"
#include "CalcUtil.h"
#include "SimpleLog.h"

struct I_IFVInfluenceMapCalc : public Action {

    I_IFVInfluenceMapCalc() : Action(false) {}

    ActionResult doAction(MyStrategy &myStrategy) override {
        if (VarStorage::currentWorld->getTickIndex() % 5) {
            return SKIP;
        }

        auto start = std::chrono::steady_clock::now();
        double divizion = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
        double (&IFVinfluenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::IFVinfluenceField;

        double (&tempLayer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[0];
        double (&layer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[1];
        double (&layer2)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[2];

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::TANK)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer1);

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer2);

        memset(IFVinfluenceField, 0,
               sizeof(IFVinfluenceField[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);

        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {
                IFVinfluenceField[i][j] = std::min(layer2[i][j] / layer1[i][j], 1.5);
                IFVinfluenceField[i][j] = std::max(IFVinfluenceField[i][j], 1. / 1.5);
            }
        }

//        memset(tempLayer1, 0, sizeof(layer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
//        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::IFV)]) {
//            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
//        }
//        CalcUtil::closeRangeSpreadField(tempLayer1, layer2);

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::closestRangeSpreadField(tempLayer1, layer1);

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::closestRangeSpreadField(tempLayer1, layer2);

        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {
                if (layer1[i][j] != 0 || layer2[i][j] != 0) {
                    double coeff = 1. + layer1[i][j] / (20. / 16.) + layer2[i][j] / (20. / 16.);
                    IFVinfluenceField[i][j] *= coeff;
                }
            }
        }

        auto now = std::chrono::steady_clock::now();
        long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
        LOG_DEBUG("% microseconds", times);
        return SKIP;
    }
};


#endif //CPP_CGDK_I_TANKINFLUENCEMAPCALC_H
