//
// Created by dragoon on 11/11/17.
//

#ifndef CPP_CGDK_PIATNASHKI_H
#define CPP_CGDK_PIATNASHKI_H


#include <utility>
#include <vector>

class Piatnashki {

public:
    static std::vector<std::vector<std::pair<int, int> >>
    getPiatnashkiMovements(const std::vector<std::pair<int, int>> &start, const std::vector<std::pair<int, int>> &dest);
};


#endif //CPP_CGDK_PIATNASHKI_H
