//
// Created by dragoon on 11/8/17.
//

#ifndef CPP_CGDK_MYVEHICLE_H
#define CPP_CGDK_MYVEHICLE_H

#include "model/VehicleType.h"
#include "model/Vehicle.h"
#include "Point.h"

class MyVehicle {
public:
    Point position;
    Point destination;
    int expectedTicksForMovement;
    int cooldownTicks;
    int durability;
    bool selected;
    bool moved;
    Point lastMove;
    model::VehicleType vType;
    std::vector<int> groups;

    MyVehicle(const model::Vehicle &vehicle);

    void updateEnValues(const model::VehicleUpdate &update);

    void updateMyValues(const model::VehicleUpdate &update);

    void updateDestination(const Point &update);

    inline double getSqrDistanceTo(const Point &point) const {
        return (point - position).sqrDistance();
    }

};


#endif //CPP_CGDK_MYVEHICLE_H
