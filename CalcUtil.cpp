//
// Created by dragoon on 08.11.17.
//

#include "CalcUtil.h"
#include "VarStorage.h"
#include "SimpleLog.h"
#include <algorithm>
#include <cstring>

model::Game CalcUtil::game;

model::VehicleType CalcUtil::startingPosition[3][3];

double CalcUtil::startCoords[4];

double CalcUtil::maxSpeed[int(model::VehicleType::_COUNT_)];

double CalcUtil::visionRange[int(model::VehicleType::_COUNT_)];

bool CalcUtil::isGroundUnit[int(model::VehicleType::_COUNT_)];

/*
 ARRV = 0,
        FIGHTER = 1,
        HELICOPTER = 2,
        IFV = 3,
        TANK = 4,

 */
int CalcUtil::damageTo[int(model::VehicleType::_COUNT_)][int(model::VehicleType::_COUNT_)] = {
        {0,  0,  0,  0,  0},
        {0,  30, 60, 0,  0},
        {80, 10, 40, 20, 40},
        {40, 10, 40, 30, 10},
        {50, 0,  20, 40, 20}
};

double CalcUtil::squadsDistance;


std::array<model::VehicleType, 5> CalcUtil::vehicleTypes = {
        model::VehicleType::TANK,
        model::VehicleType::ARRV,
        model::VehicleType::IFV,
        model::VehicleType::HELICOPTER,
        model::VehicleType::FIGHTER};

void CalcUtil::getUnitsOfType(std::vector<MyVehicle *> &vector,
                              model::VehicleType vehicleType,
                              std::vector<MyVehicle *> &dest) {
    dest.clear();
    for (auto &item : vector) {
        if (item->vType == vehicleType) {
            dest.push_back(item);
        }
    }
}


Point CalcUtil::getCenterCoords(const std::vector<MyVehicle *> &source) {
    Point pt;
    for (auto &item : source) {
        pt += item->position;
    }
    return pt / source.size();
}

double CalcUtil::getMaxDistance(const std::vector<MyVehicle *> &source, const Point &dest) {
    double maxSqrDistance = 0.;
    for (auto &item : source) {
        double dist = item->getSqrDistanceTo(dest);
        if (dist > maxSqrDistance) {
            maxSqrDistance = dist;
        }
    }
    return sqrt(maxSqrDistance);
}


double CalcUtil::getMaxRotationSpeed(const std::vector<MyVehicle *> &source, Point around) {
    double maxRotationSpeed = Constants::PI;
    for (auto &vehicle : source) {
        double speed = getMaxSpeed(vehicle->vType) * 0.6;
        double alpha = asin(speed / (2 * sqrt(vehicle->getSqrDistanceTo(around)))) * 2.;
        if (alpha < maxRotationSpeed) {
            maxRotationSpeed = alpha;
        }
    }
    return maxRotationSpeed;
}

void CalcUtil::getStartingCoords(model::VehicleType vehicleType, int &x, int &y) {
    for (int i = 0; i != 3; ++i) {
        for (int j = 0; j != 3; ++j) {
            if (CalcUtil::startingPosition[i][j] == vehicleType) {
                x = i;
                y = j;
                return;
            }
        }
    }
}

int CalcUtil::avaliableActionsCount(const MyStrategy &myStrategy) {
    if (game.getBaseActionCount() > myStrategy.lastActionsTime.size()) {
        int currentCnt = game.getBaseActionCount() - myStrategy.lastActionsTime.size();
        int from = VarStorage::currentWorld->getTickIndex() - game.getActionDetectionInterval();
        for (size_t i = 0; i != myStrategy.lastActionsTime.size() && from == myStrategy.lastActionsTime[i]; ++i) {
            ++currentCnt;
            ++from;
        }
        return currentCnt;
    } else {
        return 0;
    }

}

int CalcUtil::visAvaliableActionsCount(const MyStrategy &myStrategy) {
    int count = (int) myStrategy.lastActionsTime.size();
    if (count > 0 && *myStrategy.lastActionsTime.rbegin() == VarStorage::currentWorld->getTickIndex()) {
        --count;
    }
    if (game.getBaseActionCount() > count) {
        int currentCnt = game.getBaseActionCount() - count;
        int from = VarStorage::currentWorld->getTickIndex() - game.getActionDetectionInterval() + 1;
        for (int i = 0; i != count && from == myStrategy.lastActionsTime[i]; ++i) {
            ++currentCnt;
            ++from;
        }
        return currentCnt;
    } else {
        return 0;
    }
}

std::vector<int> CalcUtil::getCounts(const std::unordered_map<long long, MyVehicle> &vehicles) {
    std::vector<int> counts(static_cast<int>(model::VehicleType::_COUNT_));
    for (auto &vehicle : vehicles) {
        ++counts[int(vehicle.second.vType)];
    }
    return counts;
}

std::vector<MyVehicle *> const &CalcUtil::getSelectedUnits() {
    return VarStorage::selectedUnits;
}

void CalcUtil::semiLinearSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                     double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]) {
    const int influenceMapSize = Constants::influenceMapSize;
    CalcUtil::clearField(to);
    for (int i = 0; i != influenceMapSize; ++i) {
        for (int j = 0; j != influenceMapSize; ++j) {
            if (from[i][j] != 0) {
                int fromX = influenceMapSize - i - 1;
                int fromY = influenceMapSize - j - 1;
                for (int i2 = 0; i2 != influenceMapSize; ++i2) {
                    for (int j2 = 0; j2 != influenceMapSize; ++j2) {
                        to[i2][j2] += CalcUtil::globalSpreadMatrix[fromX + i2][fromY + j2] * from[i][j];
                    }
                }
            }
        }
    }
//    const double partMain = .25;
//    double partNear = 1.;
//    double partDiag = 1 / sqrt(2.);
//    const double sum = partNear * 4 + partDiag * 4;
//    partNear /= sum / partMain;
//    partDiag /= sum / partMain;
//    const double vals[] = {partDiag, partNear, partMain};
////    LOG_DEBUG("%", partMain + partNear * 4 + partDiag*4);
//
//    const int influenceMapSize = Constants::influenceMapSize;
//    CalcUtil::clearField(to);
//
//    for (int i = 0; i != influenceMapSize; ++i) {
//        for (int j = 0; j != influenceMapSize; ++j) {
//            to[i][j] += from[i][j] * vals[2];
//        }
//    }
//
//    for (int dX = -1; dX != 3; dX += 2) {
//        for (int dY = -1; dY != 3; dY += 2) {
//            int xFrom = std::max(-dX, 0);
//            int xTo = std::min(influenceMapSize - dX, influenceMapSize);
//            for (int i = xFrom; i != xTo; ++i) {
//                int yFrom = std::max(-dY, 0);
//                int yTo = std::min(influenceMapSize - dY, influenceMapSize);
//                for (int j = yFrom; j != yTo; ++j) {
//                    int toX = i + dX;
//                    int toY = j + dY;
////                    if (toX < 0 || toX >= influenceMapSize) {
////                        LOG_ERROR("oops, smth going wrong!");
////                    }
////                    if (toY < 0 || toY >= influenceMapSize) {
////                        LOG_ERROR("oops, smth going wrong!");
////                    }
//                    to[toX][toY] += from[i][j] * vals[0];
//                }
//            }
//        }
//    }
//
//    for (int dX = -1; dX != 2; ++dX) {
//        for (int dY = -1; dY != 2; ++dY) {
//            if (dY != 0 && dX != 0) {
//                continue;
//            }
//            int xFrom = std::max(-dX, 0);
//            int xTo = std::min(influenceMapSize - dX, influenceMapSize);
//            for (int i = xFrom; i != xTo; ++i) {
//                int yFrom = std::max(-dY, 0);
//                int yTo = std::min(influenceMapSize - dY, influenceMapSize);
//                for (int j = yFrom; j != yTo; ++j) {
//                    int toX = i + dX;
//                    int toY = j + dY;
////                    if (toX < 0 || toX >= influenceMapSize) {
////                        LOG_ERROR("oops, smth going wrong!");
////                    }
////                    if (toY < 0 || toY >= influenceMapSize) {
////                        LOG_ERROR("oops, smth going wrong!");
////                    }
//                    to[toX][toY] += from[i][j] * vals[1];
//                }
//            }
//        }
//    }
}

double CalcUtil::globalSpreadMatrix[Constants::influenceMapSize * 2 - 1][Constants::influenceMapSize * 2 - 1];

double CalcUtil::closeRangeSpreadMatrix[Constants::closeRangeSpread][Constants::closeRangeSpread];

double CalcUtil::closestRangeSpreadMatrix[Constants::closestRangeSpread][Constants::closestRangeSpread];

void CalcUtil::initSpreadMatrix() {

    {
        double sum = 0.;
        int to = Constants::influenceMapSize * 2 - 1;
        int mid = to / 2;
        for (int i2 = 0; i2 != to; ++i2) {
            for (int j2 = 0; j2 != to; ++j2) {
                int val = abs(mid - i2) + 1;
                int val2 = abs(mid - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                sum += (1. / dist);
            }
        }
        for (int i2 = 0; i2 != to; ++i2) {
            for (int j2 = 0; j2 != to; ++j2) {
                int val = abs(mid - i2) + 1;
                int val2 = abs(mid - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                double rez = 1. / dist;
                globalSpreadMatrix[i2][j2] = rez / sum;
            }
        }
    }

    {// close range spread
        int midC = Constants::closeRangeSpread / 2;
        double sum = 0;
        for (int i2 = 0; i2 != Constants::closeRangeSpread; ++i2) {
            for (int j2 = 0; j2 != Constants::closeRangeSpread; ++j2) {
                int val = abs(midC - i2) + 1;
                int val2 = abs(midC - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                if (dist > midC) {
                    continue;
                }
                sum += (1. / dist);
            }
        }

        for (int i2 = 0; i2 != Constants::closeRangeSpread; ++i2) {
            for (int j2 = 0; j2 != Constants::closeRangeSpread; ++j2) {
                int val = abs(midC - i2) + 1;
                int val2 = abs(midC - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                if (dist > midC) {
                    continue;
                }
                double rez = 1. / dist;
                closeRangeSpreadMatrix[i2][j2] = rez / sum;
            }
        }
    }

    {// closest range spread
        int midC = Constants::closestRangeSpread / 2;
        double sum = 0;
        for (int i2 = 0; i2 != Constants::closestRangeSpread; ++i2) {
            for (int j2 = 0; j2 != Constants::closestRangeSpread; ++j2) {
                int val = abs(midC - i2) + 1;
                int val2 = abs(midC - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                if (dist > midC) {
                    continue;
                }
                sum += (1. / dist);
            }
        }

        for (int i2 = 0; i2 != Constants::closestRangeSpread; ++i2) {
            for (int j2 = 0; j2 != Constants::closestRangeSpread; ++j2) {
                int val = abs(midC - i2) + 1;
                int val2 = abs(midC - j2) + 1;
                double dist = sqrt(val * val + val2 * val2);
                if (dist > midC) {
                    continue;
                }
                double rez = 1. / dist;
                closestRangeSpreadMatrix[i2][j2] = rez / sum;
            }
        }
    }
}

void CalcUtil::closeRangeSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                     double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]) {
    const int influenceMapSize = Constants::influenceMapSize;
    int midC = Constants::closeRangeSpread / 2;
    CalcUtil::clearField(to);
    for (int i = 0; i != influenceMapSize; ++i) {
        for (int j = 0; j != influenceMapSize; ++j) {
            if (from[i][j] != 0) {
                int xFrom = std::max(0, i - midC);
                int xTo = std::min(influenceMapSize, i + midC + 1);

                int yFrom = std::max(0, j - midC);
                int yTo = std::min(influenceMapSize, j + midC + 1);

                for (int i2 = xFrom; i2 != xTo; ++i2) {
                    int fromPosX = i2 - i + midC;
                    for (int j2 = yFrom; j2 != yTo; ++j2) {
                        int fromPosY = j2 - j + midC;
                        to[i2][j2] += CalcUtil::closeRangeSpreadMatrix[fromPosX][fromPosY] * from[i][j];
                    }
                }
            }
        }
    }
}

void CalcUtil::closestRangeSpreadField(double (&from)[Constants::influenceMapSize][Constants::influenceMapSize],
                                       double (&to)[Constants::influenceMapSize][Constants::influenceMapSize]) {
    const int influenceMapSize = Constants::influenceMapSize;
    int midC = Constants::closestRangeSpread / 2;
    CalcUtil::clearField(to);
    for (int i = 0; i != influenceMapSize; ++i) {
        for (int j = 0; j != influenceMapSize; ++j) {
            if (from[i][j] != 0) {
                int xFrom = std::max(0, i - midC);
                int xTo = std::min(influenceMapSize, i + midC + 1);

                int yFrom = std::max(0, j - midC);
                int yTo = std::min(influenceMapSize, j + midC + 1);

                for (int i2 = xFrom; i2 != xTo; ++i2) {
                    int fromPosX = i2 - i + midC;
                    for (int j2 = yFrom; j2 != yTo; ++j2) {
                        int fromPosY = j2 - j + midC;
                        to[i2][j2] += CalcUtil::closestRangeSpreadMatrix[fromPosX][fromPosY] * from[i][j];
                    }
                }
            }
        }
    }
}

bool CalcUtil::isGroupSelected(int groupNo) {
    for (auto &vehicle : VarStorage::myVehiclesVc) {
        //std::find(vehicle->groups.begin(), vehicle->groups.end(), groupNo)!= vehicle->groups.end() == selected
        if ((std::find(vehicle->groups.begin(), vehicle->groups.end(), groupNo) != vehicle->groups.end()) ^ vehicle->selected) {
            return false;
        }
    }

    return true;
}

inline int getEnemyCounts(const GroupBlocks &calcBlock,
                          const int doNothingTicksCount,
                          double distanceToShoot,
                          const std::vector<MyVehicle *> &enemyFiltered,
                          int *enemyCounts) {
    for (int x = 0; x != int(model::VehicleType::_COUNT_); ++x) {
        enemyCounts[x] = 0;
    }
    int hisTotalHealth = 0;
    for (auto &enemy : enemyFiltered) {
        double sqrDistance = CalcUtil::sqrDistancePointToSegment(enemy->position, calcBlock.currentPosMin, calcBlock.dest);
        if (sqrDistance < distanceToShoot) {
            ++enemyCounts[int(enemy->vType)];
            hisTotalHealth += enemy->durability;
            continue;
        }
        if (enemy->moved) {
            Point newPosition = enemy->position + (enemy->lastMove * doNothingTicksCount);
            sqrDistance = CalcUtil::sqrDistancePointToSegment(newPosition, calcBlock.currentPosMin, calcBlock.dest);
            if (sqrDistance < distanceToShoot) {
                ++enemyCounts[int(enemy->vType)];
                hisTotalHealth += enemy->durability;
            }
        }
    }
    return hisTotalHealth;
}

inline ExpectedMoveResult calculate(const int (&enemyCounts)[int(model::VehicleType::_COUNT_)],
                                    int hisTotalHealth,
                                    int myTotalHealth,
                                    int myCounts,
                                    int myVType,
                                    int ticksCount) {
    if (hisTotalHealth == 0) {
        return {ticksCount, 0, 0, 0, 0};
    }
    int hisDamages = 0;
    int totalCounts = 0;
    for (int i = 0; i != int(model::VehicleType::_COUNT_); ++i) {
        hisDamages += enemyCounts[i] * CalcUtil::damageTo[i][myVType];
        totalCounts += enemyCounts[i];
    }
    float totalDamage = 0;
    for (int i = 0; i != int(model::VehicleType::_COUNT_); ++i) {
        float part = enemyCounts[i] / float(totalCounts);
        totalDamage += part * myCounts * CalcUtil::damageTo[myVType][i];
    }
    auto myDamage = int(std::round(totalDamage));
    if (myDamage > hisTotalHealth) {
        myDamage = hisTotalHealth;
    }
    if (hisDamages > myTotalHealth) {
        hisDamages = myTotalHealth;
    }

    return {ticksCount,
            myDamage,
            totalCounts == 0 ? 0 : ((totalCounts * myDamage) / hisTotalHealth),
            hisDamages,
            myCounts * hisDamages / myTotalHealth};
}

ExpectedMoveResult CalcUtil::checkDirection(int direction,
                                            const std::vector<MyVehicle *> &group,
                                            GroupBlocks &moveBlock,
                                            int maxTicks) {
    int myTotalHealth = 0;
    auto myCounts = int(group.size());
    auto myVType = int(group[0]->vType);
    for (auto &myVeh : group) {
        myTotalHealth += myVeh->durability;
    }

    double distanceToShoot = (moveBlock.radius + 20) * (moveBlock.radius + 20);
    int enemyCounts[int(model::VehicleType::_COUNT_)];

    ExpectedMoveResult bestResult = ExpectedMoveResult();
    const int cnt = 10;

    double angle = CalcUtil::getAngle(direction);
    Point vector = {std::cos(angle), std::sin(angle)};
    for (int i = cnt - 1; i >= 0; --i) {
        int ticksToCalc = (maxTicks * (i + 1)) / cnt;
        double distance = moveBlock.maxSpeed * ticksToCalc;
        moveBlock.dest = moveBlock.dest + (vector * distance);
        int hisTotalHealth = getEnemyCounts(moveBlock, ticksToCalc, distanceToShoot, VarStorage::enemyVehiclesVc, enemyCounts);
        ExpectedMoveResult currentResult = calculate(enemyCounts, hisTotalHealth, myTotalHealth, myCounts, myVType, ticksToCalc);
        if (currentResult > bestResult) {
            bestResult = currentResult;
            bestResult.ticksCount = ticksToCalc;
            LOG_ERROR("try %, outcoming:% incoming:% kills:% deaths:%", i,
                      currentResult.expectedOutcoming,
                      currentResult.expectedIncoming,
                      currentResult.expectedEnemyDeaths,
                      currentResult.expectedMyDeaths);
        }
        if (hisTotalHealth == 0) {
            break;
        }
        LOG_ERROR("got %, outcoming:% incoming:% kills:% deaths:%\n===========================", i,
                  currentResult.expectedOutcoming,
                  currentResult.expectedIncoming,
                  currentResult.expectedEnemyDeaths,
                  currentResult.expectedMyDeaths);
    }
    return bestResult;
}

ExpectedMoveResult CalcUtil::checkWaiting(const std::vector<MyVehicle *> &group, const GroupBlocks &waitingBlock) {
    auto ticks = int(waitingBlock.currentPosMin.distance(waitingBlock.dest) / waitingBlock.maxSpeed);
    if (ticks < 60) {
        ticks = 60;
    }
    int myTotalHealth = 0;
    auto myCounts = int(group.size());
    auto myVType = int(group[0]->vType);
    for (auto &myVeh : group) {
        myTotalHealth += myVeh->durability;
    }

    double distanceToShoot = (waitingBlock.radius + 20) * (waitingBlock.radius + 20);
    int enemyCounts[int(model::VehicleType::_COUNT_)];
    int hisTotalHealth = getEnemyCounts(waitingBlock, ticks, distanceToShoot, VarStorage::enemyVehiclesVc, enemyCounts);
    return calculate(enemyCounts, hisTotalHealth, myTotalHealth, myCounts, myVType, ticks);
}

