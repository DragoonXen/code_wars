//
// Created by dragoon on 11/11/17.
//

#ifndef CPP_CGDK_SIGNALACTION_H
#define CPP_CGDK_SIGNALACTION_H


#include "Action.h"

struct SignalAction : public Action {
    int signals = 0;

    ActionResult doSmth(MyStrategy &myStrategy) {
        if (signals) {
            return SKIP;
        }
        return doAction(myStrategy);
    }

    virtual ActionResult doAction(MyStrategy &myStrategy) = 0;
};


#endif //CPP_CGDK_SIGNALACTION_H
