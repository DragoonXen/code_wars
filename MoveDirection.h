//
// Created by dragoon on 11/18/17.
//

#ifndef CPP_CGDK_MOVEDIRECTION_H
#define CPP_CGDK_MOVEDIRECTION_H

struct MoveDirection {
    double score;
    double danger;
    double positive;
    int angle;

    MoveDirection(double score, double danger, double positive, int angle) : score(score), danger(danger), positive(positive), angle(angle) {}
};

#endif //CPP_CGDK_MOVEDIRECTION_H

