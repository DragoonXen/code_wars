#pragma once

#ifndef _MY_STRATEGY_H_
#define _MY_STRATEGY_H_

#include <unordered_map>
#include <unordered_set>
#include <functional>
#include "Strategy.h"
#include "MyVehicle.h"
#include "Action.h"
#include "GroupBlocks.h"

#include <memory>
#include <list>

class MyStrategy : public Strategy {
private:
    void prepareUnitsList(const model::World &world);

    void moveBlocks();

    void initConstants(const model::Game &game);

    void calculating(const model::Player &me, const model::World &world, const model::Game &game, model::Move &move);

public:
    void addActionBack(const std::shared_ptr<Action> &action) {
        actions.push_back(action);
    }

    void addActionAfter(std::list<std::shared_ptr<Action>>::iterator iterator, const std::shared_ptr<Action> &action) {
        actions.insert(iterator, action);
    }

    std::vector<int> sgn;
    std::list<std::shared_ptr<Action>> actions;
    std::unordered_map<long long, MyVehicle> myVehicles;
    std::unordered_map<long long, MyVehicle> enemyVehicles;
    std::unordered_set<long long> myVehiclesIds;
    std::vector<int> lastActionsTime;
    std::vector<MyVehicle *> groups[101];
    std::vector<GroupBlocks> blocks;

public:
    MyStrategy();

    void move(const model::Player &me, const model::World &world, const model::Game &game, model::Move &move) override;
};

#endif
