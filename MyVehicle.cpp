//
// Created by dragoon on 11/8/17.
//

#include "MyVehicle.h"

MyVehicle::MyVehicle(const model::Vehicle &vehicle)
        : position(Point(vehicle.getX(), vehicle.getY())),
          cooldownTicks(0),
          durability(vehicle.getDurability()),
          selected(vehicle.isSelected()),
          vType(vehicle.getType()) {
}

void MyVehicle::updateEnValues(const model::VehicleUpdate &update) {
    this->cooldownTicks = update.getRemainingAttackCooldownTicks();
    this->durability = update.getDurability();
    this->selected = update.isSelected();
    Point updatePosition = {update.getX(), update.getY()};
    if (!this->position.equalsValues(updatePosition)) {
        this->moved = true;
        this->lastMove = this->position - updatePosition;
        this->position = updatePosition;
    }
}

void MyVehicle::updateMyValues(const model::VehicleUpdate &update) {
    this->cooldownTicks = update.getRemainingAttackCooldownTicks();
    this->durability = update.getDurability();
    this->selected = update.isSelected();
    Point updatePosition = Point(update.getX(), update.getY());
    if (!this->position.equalsValues(updatePosition)) {
        this->moved = true;
        this->lastMove = this->position - updatePosition;
        double distance = (this->destination - updatePosition).distance();
        double moveDistance = (this->position - updatePosition).distance();
        this->expectedTicksForMovement = int((distance + moveDistance) / moveDistance - 1e-6);
        this->position = updatePosition;
    }
}

void MyVehicle::updateDestination(const Point &update) {
    this->destination = update;
}

