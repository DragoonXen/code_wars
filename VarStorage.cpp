//
// Created by dragoon on 11/9/17.
//

#include "VarStorage.h"


const model::World *VarStorage::currentWorld;
const model::Player *VarStorage::currentPlayer;
const model::Player *VarStorage::opponentPlayer;
model::Move *VarStorage::currentMove;

std::vector<GroupBlocks> VarStorage::groupBlocks;

std::vector<MyVehicle *> VarStorage::selectedUnits;

double VarStorage::jetInfluenceField[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize];
double VarStorage::heliInfluenceField[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize];
double VarStorage::tankInfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];
double VarStorage::IFVinfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];
double VarStorage::ARRVInfluenceField[Constants::influenceMapSize][Constants::influenceMapSize];

double VarStorage::influenceFieldTemp[10][Constants::influenceMapSize][Constants::influenceMapSize];

std::vector<MyVehicle *> VarStorage::myVehiclesVc;
std::vector<MyVehicle *> VarStorage::enemyVehiclesVc;

std::vector<MyVehicle *> VarStorage::myVehiclesByType[int(model::VehicleType::_COUNT_)];
std::vector<MyVehicle *> VarStorage::enemyVehiclesByType[int(model::VehicleType::_COUNT_)];