//
// Created by dragoon on 11/13/17.
//

#ifndef CPP_CGDK_HELIINFLUENCEMAPCALC_H
#define CPP_CGDK_HELIINFLUENCEMAPCALC_H

#include <cstring>
#include <chrono>
#include <algorithm>
#include "Action.h"
#include "MyStrategy.h"
#include "CalcUtil.h"
#include "SimpleLog.h"

struct I_HeliInfluenceMapCalc {

    static void calculateHeliField(MyStrategy &myStrategy) {
        auto start = std::chrono::steady_clock::now();
        double divizion = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
        double (&heliInfluenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::heliInfluenceField[0];

        double (&coverField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::heliInfluenceField[1];
        double (&attackField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::heliInfluenceField[2];
        double (&defenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::heliInfluenceField[3];

        double (&tempLayer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[0];
        double (&layer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[1];
        double (&layer2)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[2];

        CalcUtil::clearField(coverField);
        if (!VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)].empty() &&
            !VarStorage::myVehiclesByType[int(model::VehicleType::FIGHTER)].empty()) {
            CalcUtil::clearField(tempLayer1);
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
            }
            CalcUtil::semiLinearSpreadField(tempLayer1, layer1);

            CalcUtil::clearField(tempLayer1);
            for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
            }
            CalcUtil::semiLinearSpreadField(tempLayer1, layer2);


            for (int i = 0; i != Constants::influenceMapSize; ++i) {
                for (int j = 0; j != Constants::influenceMapSize; ++j) {
                    coverField[i][j] = std::min(layer2[i][j] / layer1[i][j], 1.5);
                    coverField[i][j] = std::max(coverField[i][j], 1. / 1.5);
                }
            }
        }

        CalcUtil::clearField(tempLayer1);
        {
            const int damage = CalcUtil::game.getIfvAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::IFV)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        {
            const int damage = CalcUtil::game.getFighterAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        {
            const int damage = CalcUtil::game.getTankAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::TANK)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += damage;
            }
        }
        CalcUtil::closestRangeSpreadField(tempLayer1, defenceField);
        CalcUtil::semiLinearSpreadField(tempLayer1, VarStorage::heliInfluenceField[4]);// danger meter

        CalcUtil::clearField(tempLayer1);
        {
            const int damage = CalcUtil::game.getHelicopterAerialDamage() - CalcUtil::game.getHelicopterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterAerialDamage() - CalcUtil::game.getFighterAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::FIGHTER)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterGroundDamage() - CalcUtil::game.getTankAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::TANK)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterGroundDamage() - CalcUtil::game.getIfvAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::IFV)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        {
            const int damage = CalcUtil::game.getHelicopterGroundDamage() - CalcUtil::game.getArrvAerialDefence();
            for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::ARRV)]) {
                tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 100. / ((unit->durability + damage - 1) / damage);
            }
        }
        CalcUtil::closeRangeSpreadField(tempLayer1, attackField);
        CalcUtil::semiLinearSpreadField(tempLayer1, layer2);

        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {
                heliInfluenceField[i][j] = layer2[i][j] * 1e-7
                                           + 200. * coverField[i][j]
                                           + 1.5 * attackField[i][j]
                                           - .32 * defenceField[i][j];
            }
        }
        auto now = std::chrono::steady_clock::now();
        long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
        LOG_DEBUG("% microseconds", times);
    }
};


#endif //CPP_CGDK_HELIINFLUENCEMAPCALC_H
