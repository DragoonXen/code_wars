//
// Created by dragoon on 08.11.17.
//

#ifndef CPP_CGDK_POINT_H
#define CPP_CGDK_POINT_H

#include <cmath>

struct Point {
    double x, y;

    Point(double x, double y) : x(x), y(y) {}

    Point() : x(0), y(0) {}

    inline Point operator-(const Point &other) const {
        return {this->x - other.x, this->y - other.y};
    }

    Point &operator-=(const Point &other) {
        this->x -= other.x;
        this->y -= other.y;
        return *this;
    }

    Point operator+(const Point &other) const {
        return {this->x + other.x, this->y + other.y};
    }

    Point &operator+=(const Point &other) {
        this->x += other.x;
        this->y += other.y;
        return *this;
    }

    Point operator/(const double value) const {
        return {this->x / value, this->y / value};
    }

    Point &operator/=(const double value) {
        this->x /= value;
        this->y /= value;
        return *this;
    }

    Point operator*(const double value) const {
        return {this->x * value, this->y * value};
    }

    Point operator*=(const double value) {
        this->x *= value;
        this->y *= value;
        return *this;
    }

    Point operator*(const Point &other) const {
        return {this->x * other.x, this->y * other.y};
    }

    Point &rotate(double angle) {
        double newX = x * cos(angle) - y * sin(angle);
        this->y = x * sin(angle) + y * cos(angle);
        this->x = newX;
        return *this;
    }

    double sqrDistance() const {
        return this->x * this->x + this->y * this->y;
    }

    double distance() const {
        return sqrt(this->x * this->x + this->y * this->y);
    }

    double distance(const Point &other) const {
        double val = this->x - other.x;
        double val2 = this->y - other.y;
        return sqrt(val * val + val2 * val2);
    }

    double sqrDistance(const Point &other) const {
        double val = this->x - other.x;
        double val2 = this->y - other.y;
        return val * val + val2 * val2;
    }

    double scalarMult(const Point &other) const {
        return x * other.x + y * other.y;
    }

    inline bool equalsValues(const Point &other) const {
        return this->x == other.x && this->y == other.y;
    }
};


#endif //CPP_CGDK_POINT_H
