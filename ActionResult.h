//
// Created by dragoon on 11/12/17.
//

#include "ResultType.h"

#ifndef CPP_CGDK_ACTIONRESULT_H
#define CPP_CGDK_ACTIONRESULT_H

#endif //CPP_CGDK_ACTIONRESULT_H

struct ActionResult {

    ResultType result;
    int count;

    ActionResult(ResultType result) : result(result) { this->count = 1; }

    ActionResult(ResultType result, int count) : result(result), count(count) {}

    bool isDone() const {
        return result == DONE || result == ACTION_SET;
    }

};