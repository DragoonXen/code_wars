//
// Created by dragoon on 16.11.17.
//

#ifndef CPP_CGDK_GROUPBLOCKS_H
#define CPP_CGDK_GROUPBLOCKS_H


#include "Point.h"

struct GroupBlocks {
    int groupId;
    bool ground;
    Point currentPosMin;
    Point currentPosMax;
    double radius;
    Point dest;
    Point onePointVector;
    double minSpeed, maxSpeed;

    GroupBlocks() {}

    GroupBlocks(int groupId,
                bool ground,
                const Point &center,
                double radius,
                double minSpeed,
                double maxSpeed)
            : groupId(groupId), ground(ground),
              currentPosMin(center),
              currentPosMax(center), radius(radius),
              dest(center), minSpeed(minSpeed),
              maxSpeed(maxSpeed) {}
};


#endif //CPP_CGDK_GROUPBLOCKS_H
