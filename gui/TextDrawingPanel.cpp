//
// Created by dragoon on 27.10.17.
//

#include <iostream>
#include "TextDrawingPanel.h"

TextDrawingPanel::TextDrawingPanel(QWidget *parent, DrawingDataStorage *drawingDataStorage) : ForceUpdatedWidget(
        parent),
                                                                                              drawingDataStorage(
                                                                                                      drawingDataStorage) {

}

void TextDrawingPanel::paintEvent(QPaintEvent *event) {
    auto drawingData = drawingDataStorage->getCurrentDrawingData();
    if (drawingData != nullptr) {
        std::vector<std::string> *lines = drawingData->getLines();

        QRectF rect = QRectF(QPointF(0, 0), this->size());
        rect.setWidth(rect.width() - 1);
        QPainter painter(this);
        painter.setBrush(Qt::white);
        painter.setPen(Qt::gray);
        painter.drawRect(rect);
        painter.setPen(Qt::black);
        painter.setBrush(QColor(240, 240, 240, 255));

        rect.setX(1);
        bool odd = true;
        for (auto &item : *lines) {
            QString str = QString::fromStdString(item);
            QRectF rectF = painter.boundingRect(rect, Qt::AlignLeft | Qt::AlignTop | Qt::TextWrapAnywhere, str);
            rect.setY(rect.y() + rectF.height());
            rect.setHeight(rect.height() - rectF.height());
            if (odd ^= true) {
                QRectF lineBackground = rectF;
                lineBackground.setWidth(rect.width());
                painter.setPen(Qt::NoPen);
                painter.drawRect(lineBackground);
                painter.setPen(Qt::black);
            }
            painter.drawText(rectF, Qt::AlignLeft | Qt::AlignTop | Qt::TextWrapAnywhere, str);
        }
        //painter.setBrush(Qt::black);
    }
    ForceUpdatedWidget::paintEvent(event);
}
