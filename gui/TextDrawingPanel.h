//
// Created by dragoon on 27.10.17.
//

#ifndef CPP_CGDK_TEXTDRAWINGPANEL_H
#define CPP_CGDK_TEXTDRAWINGPANEL_H


#include <QtWidgets/QWidget>
#include "DrawingDataStorage.h"
#include "ForceUpdatedWidget.h"

class TextDrawingPanel : public ForceUpdatedWidget {

Q_OBJECT

private:
    DrawingDataStorage *drawingDataStorage;
public:
    TextDrawingPanel(QWidget *parent, DrawingDataStorage *drawingDataStorage);

protected:
    void paintEvent(QPaintEvent *event) override;
};


#endif //CPP_CGDK_TEXTDRAWINGPANEL_H
