#ifndef CG_MAINWINDOW_H
#define CG_MAINWINDOW_H

#include <QMainWindow>
#include <QtWidgets/QSlider>
#include <QtWidgets/QGraphicsScene>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QGraphicsView>
#include "MyStrategy.h"
#include "model/World.h"
#include "TickData.h"
#include "CoordDrawingHelper.h"
#include "DrawingData.h"
#include "DrawPanel.h"
#include "TextDrawingPanel.h"
#include "FpsMeasurer.h"
#include <mutex>
#include <model/PlayerContext.h>
#include <condition_variable>

namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow {
Q_OBJECT
private:
    static const std::array<int, 11> framesPerSecond;

    static const std::array<const char *, 5> vehicleTypes;

public:
    std::mutex debugMutex;

    static MainWindow *getInstance();

    explicit MainWindow(QWidget *parent = 0);

    void initConstants(model::Game game);

    ~MainWindow();

    void prepareTickData(const model::PlayerContext &context, const MyStrategy &strategy);

    void updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy, const model::Move &move);

    void finishTick();

    void readyToNextFrame();

private slots:

    void runRepaint();

    void changeDrawingTick(int changedValue);

protected:

    void resizeEvent(QResizeEvent *evt) override;

    void keyPressEvent(QKeyEvent *event) override;

    void wheelEvent(QWheelEvent *event) override;

    void mousePressEvent(QMouseEvent *event) override;

    void mouseReleaseEvent(QMouseEvent *event) override;

    void mouseMoveEvent(QMouseEvent *event) override;

private:
    int sliderRangeStart;
    int sliderRangeEnd;
    int sliderValue;

    QPoint middleMousePressPoint = QPoint(-1000, -1000);
    long long descriptionUnitId;

    int millisToNextDraw; // 30 fps
    int minWaitBetweenDrawings;
    std::chrono::milliseconds chronoMillisToNextDraw;

    std::chrono::steady_clock::time_point currentFrameTime;
    QSlider *slider;
    bool drawLast;
    int lastPaintedTick;
    size_t lastDrawingDataTick;

    size_t selectedSimulationSpeed = 10;
    size_t prevSelectedSimulationSpeed = 0; // for pausing
    bool nextSim = false;
    std::chrono::steady_clock::time_point lastFrameSimulated;

    static MainWindow *instance;
    const int TEXT_AREA_WIDTH = 300;
    Ui::MainWindow *ui;

    bool drawBackground = true;
    int drawJetInfluence = 0;
    int drawHeliInfluence = 0;
    bool drawTankInfluence = false;
    bool drawIFVInfluence = false;
    bool drawARRVInfluence = false;

    int drawBlocks = 4;

    CoordDrawingHelper *coordDrawingHelper;

    DrawingDataStorage *drawingDataStorage;
    DrawPanel *drawing;
    TextDrawingPanel *textBrowser;
    FpsMeasurer drawingFpsMeasurer;
    FpsMeasurer strategyFpsMeasurer;

    std::vector<TickData *> ticksDataStore;
    TickData *currentTickData;
    DrawingData *strategyDrawingData = nullptr;
    model::Game game;

    MyStrategy *debugStrategy;

    void updatePaintData(int tickNom, DrawingData *drawingData);

    int keyboardModifiersSliderShift(const Qt::KeyboardModifiers &modifiers);

    void prepareTickData(TickData *const tickData, DrawingData *const whereToStore);

    void updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy, TickData *const tickData, DrawingData *const whereToStore, const model::Move &move);

    void drawField(DrawingData *const whereToStore,
                   double (&paintInfluenceField)[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize],
                   int type);

    void finishTick(TickData *const tickData, DrawingData *const whereToStore);

    void fullRedrawFrame();
};


#endif //CG_MAINWINDOW_H
