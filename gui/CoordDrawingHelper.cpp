//
// Created by dragoon on 10/21/17.
//

#include <iostream>
#include "CoordDrawingHelper.h"

CoordDrawingHelper::CoordDrawingHelper(double startX, double startY, double endX, double endY, int width, int height) :
        startX(startX),
        startY(startY),
        endX(endX),
        endY(endY),
        width(width),
        height(height) {

    cX = (endX - startX) / 2.;
    cY = (endY - startY) / 2.;
    pPC = (height * 1.) / (endY - startY);
    fitWindow();
}

void CoordDrawingHelper::zoomIn() {
    zoomIn(0.5, 0.5);
}

void CoordDrawingHelper::zoomIn(double dPointX, double dPointY) {
    double lastX = windowX + width / pPC;
    cX = windowX + (lastX - windowX) * dPointX;
    double lastY = windowY + height / pPC;
    cY = windowY + (lastY - windowY) * dPointY;

    pPC *= zoomStep;
    if (pPC > maxZoom) {
        pPC = maxZoom;
    }
    fitWindow();
}

void CoordDrawingHelper::zoomOut() {
    zoomOut(0.5, 0.5);
}

void CoordDrawingHelper::zoomOut(double dPointX, double dPointY) {
    double lastX = windowX + width / pPC;
    cX = windowX + (lastX - windowX) * dPointX;
    double lastY = windowY + height / pPC;
    cY = windowY + (lastY - windowY) * dPointY;

    pPC /= zoomStep;
    fitWindow();
}

double CoordDrawingHelper::fixX(double xCoord) const {
    return (xCoord - windowX) * pPC;
}

double CoordDrawingHelper::fixY(double yCoord) const {
    return (yCoord - windowY) * pPC;
}

QPointF CoordDrawingHelper::screenToOrig(const QPoint &pos) const {
    return QPointF(pos.x() / pPC + windowX, pos.y() / pPC + windowY);
}

double CoordDrawingHelper::fixSize(double size) const {
    return size * pPC;
}

void CoordDrawingHelper::resizeDrawingWindow(int newWidth, int newHeight) {
    std::cout << newWidth << ' ' << newHeight << std::endl;
    this->height = newHeight;
    this->width = newWidth;
    fitWindow();
}

void CoordDrawingHelper::fitWindow() {
    double pPX = (width * 1.) / (endX - startX);
    double pPY = (height * 1.) / (endY - startY);
    double checkPPC = std::min(pPX, pPY);
    if (checkPPC > pPC) {
        pPC = checkPPC;
    }
    std::cout << pPC << std::endl;
    std::cout << checkPPC << std::endl;

    windowX = cX - width / (pPC * 2.);
    double lastX = windowX + width / pPC;
    if ((lastX - windowX) >= (endX - startX)) {
        cX = (endX - startX) / 2.;
        windowX = cX - width / (pPC * 2.);
    } else {
        if (windowX < startX) {
            cX += startX - windowX;
            windowX = 0;
        } else if (lastX > endX) {
            cX -= lastX - endX;
            windowX -= lastX - endX;
        }
    }

    windowY = cY - height / (pPC * 2.);
    double lastY = windowY + height / pPC;
    if ((lastY - windowY) >= (endY - startY)) {
        cY = (endY - startY) / 2.;
        windowY = cY - height / (pPC * 2.);
    } else {
        if (windowY < startY) {
            cY += startY - windowY;
            windowY = 0;
        } else if (lastY > endY) {
            cY -= lastY - endY;
            windowY -= lastY - endY;
        }
    }
}

void CoordDrawingHelper::scroll(CoordDrawingHelper::Direction direction) {
    double lastCoord;
    switch (direction) {
        case UP:
            lastCoord = windowY + height / pPC;
            cY = windowY + (lastCoord - windowY) * (.5 - scrollSpeed);
            break;
        case DOWN:
            lastCoord = windowY + height / pPC;
            cY = windowY + (lastCoord - windowY) * (.5 + scrollSpeed);
            break;
        case LEFT:
            lastCoord = windowX + width / pPC;
            cX = windowX + (lastCoord - windowX) * (.5 - scrollSpeed);
            break;
        case RIGHT:
            lastCoord = windowX + width / pPC;
            cX = windowX + (lastCoord - windowX) * (.5 + scrollSpeed);
            break;
    }
    fitWindow();
}

void CoordDrawingHelper::scroll(QPoint direction) {
    cX -= direction.x() / pPC;
    cY -= direction.y() / pPC;
    fitWindow();
}

int CoordDrawingHelper::getWidth() const {
    return width;
}

int CoordDrawingHelper::getHeight() const {
    return height;
}



