//
// Created by dragoon on 26.10.17.
//

#include "DrawPanel.h"

void DrawPanel::updateHelperData(CoordDrawingHelper *coordDrawingHelper) {
    this->coordDrawingHelper = coordDrawingHelper;
}

void DrawPanel::paintEvent(QPaintEvent *event) {
    QPainter painter(this);
//    painter.begin(this->viewport());
    painter.setPen(Qt::black);
    painter.setBrush(Qt::white);
    QPolygonF poly(QRectF(this->coordDrawingHelper->fixX(0.),
                          this->coordDrawingHelper->fixY(0.),
                          this->coordDrawingHelper->fixX(1024.) - this->coordDrawingHelper->fixX(0.),
                          this->coordDrawingHelper->fixY(1024.) - this->coordDrawingHelper->fixY(0.)));
    painter.drawConvexPolygon(poly);
    DrawingData *drawingData = this->drawingDataStorage->getCurrentDrawingData();
    if (drawingData != nullptr) {
        drawingData->drawAll(painter, *(this->coordDrawingHelper));
    }
    painter.end();
    this->updated = true;
    ForceUpdatedWidget::paintEvent(event);
}

DrawPanel::DrawPanel(QWidget *parent, DrawingDataStorage *drawingDataStorage) : ForceUpdatedWidget(parent),
                                                                                drawingDataStorage(drawingDataStorage) {
}