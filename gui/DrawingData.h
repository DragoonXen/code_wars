//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_DRAWINGDATA_H
#define CPP_CGDK_DRAWINGDATA_H


#include <gui/drawingModel/Figure.h>
#include <vector>
#include <QtGui/QPainter>
#include <memory>
#include "TickData.h"
#include "CoordDrawingHelper.h"

#ifdef WITH_VISUALIZER

#include <QtCore/QtCore>

#endif

class DrawingData {

private:
    std::vector<std::unique_ptr<Figure>> figures;
    TickData *tickData;
    std::vector<std::string> lines;

public:
    DrawingData(TickData *tickData);

    void putLine(std::string line, size_t position);

    std::vector<std::string> *getLines();

    void drawAll(QPainter &painter, const CoordDrawingHelper &drawingHelper);

    void addFigure(std::unique_ptr<Figure> figure);

    const TickData *getTickData() const;
};

#ifdef WITH_VISUALIZER

Q_DECLARE_METATYPE(DrawingData*);
#endif



#endif //CPP_CGDK_DRAWINGDATA_H
