//
// Created by dragoon on 27.10.17.
//

#include "DrawingDataStorage.h"


void DrawingDataStorage::updateDrawingData(DrawingData *strategyDrawingData) {
    mutex.lock();
    DrawingItem *item = new DrawingItem(strategyDrawingData, currentDrawingDataItem);
    this->currentDrawingDataItem = item;
    mutex.unlock();
}

std::mutex DrawingDataStorage::mutex;
void DrawingDataStorage::selectAndCleanDrawingData() {
    mutex.lock();
    if (currentDrawingDataItem == nullptr) {
        currentDrawingData = nullptr;
        mutex.unlock();
        return;
    }
    DrawingItem *item = currentDrawingDataItem->nextItem;
    currentDrawingDataItem->nextItem = nullptr;
    while (item != nullptr) {
        delete item->drawingData;
        DrawingItem *nextItem = item->nextItem;
        delete item;
        item = nextItem;
    }
    currentDrawingData = currentDrawingDataItem->drawingData;
    mutex.unlock();
}

DrawingData *DrawingDataStorage::getCurrentDrawingData() const {
    return currentDrawingData;
}

DrawingDataStorage::DrawingItem::DrawingItem(DrawingData *drawingData, DrawingItem *item) : drawingData(drawingData),
                                                                                            nextItem(item) {}
