//
// Created by dragoon on 04.08.17.
//

#include <ui_MainWindow.h>
#include "MainWindow.h"
#include <QtWidgets>
#include <gui/drawingModel/Circle.h>
#include <chrono>
#include <gui/drawingModel/StatusBar.h>
#include <gui/drawingModel/TexturePainter.h>
#include <gui/drawingModel/Rect.h>
#include <gui/drawingModel/MidColorUtil.h>
#include <thread>
#include <sstream>
#include <iomanip>
#include <iostream>
#include <CalcUtil.h>
#include <gui/drawingModel/Line.h>
#include <SimpleLog.h>

using namespace std;

MainWindow *MainWindow::instance;

const std::array<int, 11> MainWindow::framesPerSecond = {0, 1, 5, 10, 15, 30, 45, 60, 90, 120, 1000};

const std::array<const char *, 5> MainWindow::vehicleTypes = {"ARRV", "FIGHTER", "HELICOPTER", "IFV", "TANK"};

MainWindow::MainWindow(QWidget *parent) :
        QMainWindow(parent), ui(new Ui::MainWindow) {

    millisToNextDraw = 1000 / 30; // 30 fps
    minWaitBetweenDrawings = millisToNextDraw / 2; // minimum half of time for ui service tasks
    chronoMillisToNextDraw = std::chrono::milliseconds(millisToNextDraw);

    ui->setupUi(this);
    setMinimumSize(500, 300);

    this->drawingDataStorage = new DrawingDataStorage;
    this->drawing = new DrawPanel(findChild<QWidget *>("centralWidget"), this->drawingDataStorage);
    this->drawing->setObjectName(QStringLiteral("drawPanel"));
    this->drawing->setGeometry(QRect(10, 10, 256, 192));

    this->textBrowser = new TextDrawingPanel(findChild<QWidget *>("centralWidget"), this->drawingDataStorage);
    this->textBrowser->setObjectName(QStringLiteral("textBrowser"));
    this->textBrowser->setGeometry(QRect(270, 10, 150, 192));
    this->textBrowser->setStyleSheet("background-color:white;");

    MainWindow::instance = this;

    slider = findChild<QSlider *>("horizontalSlider");
    slider->setMinimum(0);
    slider->setMaximum(-1);
    slider->setValue(0);

    QObject::connect(slider, SIGNAL(valueChanged(int)),
                     this, SLOT(changeDrawingTick(int)),
                     Qt::ConnectionType::DirectConnection);

    this->drawLast = true;
    this->lastPaintedTick = -2;
    grabKeyboard();

    QTimer::singleShot(millisToNextDraw, Qt::TimerType::CoarseTimer, this, SLOT(runRepaint()));
    currentFrameTime = std::chrono::steady_clock::now() + std::chrono::milliseconds(millisToNextDraw);
    lastFrameSimulated = std::chrono::steady_clock::now();

    coordDrawingHelper = new CoordDrawingHelper(0, 0, 1024, 1024, this->drawing->geometry().width(),
                                                this->drawing->geometry().height());
    TexturePainter::initTextures();

    this->sliderRangeStart = this->slider->minimum();
    this->sliderRangeEnd = this->slider->maximum();
    this->sliderValue = this->slider->value();
}

void MainWindow::initConstants(model::Game game) {
    this->game = game;
    CoordDrawingHelper *newCoordDrawingHelper = new CoordDrawingHelper(0,
                                                                       0,
                                                                       game.getWorldWidth(),
                                                                       game.getWorldHeight(),
                                                                       this->drawing->geometry().width(),
                                                                       this->drawing->geometry().height());
    CoordDrawingHelper *bkp = coordDrawingHelper;
    coordDrawingHelper = newCoordDrawingHelper;
    delete bkp;

    this->debugStrategy = new MyStrategy();
}


void MainWindow::resizeEvent(QResizeEvent *evt) {
    this->lastPaintedTick = -2;
//    cout << size().width() << ' ' << size().height() << endl;

    {
        const QRect sliderRect = slider->geometry();
        int sliderY = size().height() - 50;
        slider->setGeometry(sliderRect.x(), sliderY, size().width() - 20, sliderRect.height());
        slider->updateGeometry();

        const QRect textRect = textBrowser->geometry();
        textBrowser->setGeometry(size().width() - TEXT_AREA_WIDTH, textRect.y(), TEXT_AREA_WIDTH - 5,
                                 sliderY - textRect.y());
        textBrowser->updateGeometry();

        const QRect drawingRect = this->drawing->geometry();
        this->drawing->setGeometry(drawingRect.x(), drawingRect.y(),
                                   size().width() - TEXT_AREA_WIDTH - 5 - drawingRect.x(),
                                   sliderY - drawingRect.y());
        this->drawing->updateGeometry();
        QRect sceneRect = this->drawing->rect();
        sceneRect.setWidth(sceneRect.width() - 2);
        sceneRect.setHeight(sceneRect.height() - 2);
        this->coordDrawingHelper->resizeDrawingWindow(sceneRect.width(), sceneRect.height());
    }
}


MainWindow::~MainWindow() {
    delete ui;
}

template<typename... Args>
std::string format(const char *fmt, Args... args) {
    char buf[1000];
    snprintf(buf, 1000, fmt, args...);
    return buf;
}

void MainWindow::updatePaintData(int tickNom, DrawingData *drawingData) {
    if (drawingData != nullptr) {
        drawingData->putLine(std::to_string(tickNom) + "/" + std::to_string(slider->maximum()), 0);
        int designedFps = framesPerSecond[selectedSimulationSpeed];
        double drawingFps = this->drawingFpsMeasurer.getFps();
        double strategyFps = this->strategyFpsMeasurer.getFps();
        drawingData->putLine(format("process: %d, strategy: %.2f\ndrawing: %.2f", designedFps, strategyFps, drawingFps),
                             1);
    }
//    textBrowser->setHtml(stringLinesHtml->getAsHtml());
    lastPaintedTick = tickNom;
    lastDrawingDataTick = this->ticksDataStore.size();
}

MainWindow *MainWindow::getInstance() {
    return MainWindow::instance;
}

void MainWindow::keyPressEvent(QKeyEvent *event) {
    std::cout << "key pressed " << event->key() << std::endl;
    switch (event->key()) {
        case Qt::Key_Plus:
            if (selectedSimulationSpeed + 1 < framesPerSecond.size()) {
                ++selectedSimulationSpeed;
            }
            break;
        case Qt::Key_Minus:
            if (selectedSimulationSpeed > 0) {
                --selectedSimulationSpeed;
            }
            break;
        case Qt::Key_Space:
            if (this->selectedSimulationSpeed == 0) {
                this->selectedSimulationSpeed = this->prevSelectedSimulationSpeed;
            } else {
                this->prevSelectedSimulationSpeed = this->selectedSimulationSpeed;
                this->selectedSimulationSpeed = 0;
            }
            break;
        case ']':
            if (!this->drawLast) {
                this->drawLast = true;
                changeDrawingTick(this->slider->maximum());
            }
            break;
        case Qt::Key_Less:
        case Qt::Key_Comma:
            this->drawLast = false;
            if (this->slider->value() > 0) {
                int shift = keyboardModifiersSliderShift(event->modifiers());
                changeDrawingTick(this->slider->value() - shift);
            }
            break;
        case Qt::Key_Greater:
        case Qt::Key_Period:
            if (drawLast && selectedSimulationSpeed == 0) {
                nextSim = true;
            } else {
                this->drawLast = false;
                if (this->slider->value() < this->slider->maximum()) {
                    int shift = keyboardModifiersSliderShift(event->modifiers());
                    changeDrawingTick(this->slider->value() + shift);
                }
            }
            break;
        case Qt::Key_P:
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Up:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::UP);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Down:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::DOWN);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Left:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::LEFT);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Right:
            this->coordDrawingHelper->scroll(CoordDrawingHelper::Direction::RIGHT);
            this->lastPaintedTick = -2;
            break;
        case Qt::Key_Q:
            this->drawBlocks = (this->drawBlocks + 1) % 4;
            fullRedrawFrame();
            break;
        case Qt::Key_B:
            this->drawBackground ^= true;
            this->drawJetInfluence = 0;
            this->drawHeliInfluence = 0;
            this->drawTankInfluence = false;
            this->drawIFVInfluence = false;
            this->drawARRVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_J:
            this->drawJetInfluence = (this->drawJetInfluence + 1) % (Constants::fieldsCount + 1);
            this->drawBackground = (this->drawJetInfluence == 0);
            this->drawTankInfluence = false;
            this->drawHeliInfluence = 0;
            this->drawIFVInfluence = false;
            this->drawARRVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_H:
            this->drawHeliInfluence = (this->drawHeliInfluence + 1) % (Constants::fieldsCount + 1);
            this->drawBackground = (this->drawHeliInfluence == 0);
            this->drawJetInfluence = 0;
            this->drawTankInfluence = false;
            this->drawIFVInfluence = false;
            this->drawARRVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_T:
            this->drawTankInfluence ^= true;
            this->drawBackground = !this->drawTankInfluence;
            this->drawJetInfluence = 0;
            this->drawHeliInfluence = 0;
            this->drawIFVInfluence = false;
            this->drawARRVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_I:
            this->drawIFVInfluence ^= true;
            this->drawBackground = !this->drawIFVInfluence;
            this->drawTankInfluence = false;
            this->drawJetInfluence = 0;
            this->drawHeliInfluence = 0;
            this->drawARRVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_A:
            this->drawARRVInfluence ^= true;
            this->drawBackground = !this->drawARRVInfluence;
            this->drawTankInfluence = false;
            this->drawJetInfluence = 0;
            this->drawHeliInfluence = false;
            this->drawIFVInfluence = false;
            fullRedrawFrame();
            break;
        case Qt::Key_Return:
        case Qt::Key_Enter:
            fullRedrawFrame();
            break;
    }
    QWidget::keyPressEvent(event);
}

void MainWindow::changeDrawingTick(int changedValue) {
    this->slider->blockSignals(true);
    this->slider->setValue(changedValue);
    this->slider->blockSignals(false);
    if (this->drawLast && changedValue != this->slider->maximum()) {
        this->drawLast = false;
    }
    if (changedValue < 0) {
        changedValue = 0;
    } else if (changedValue > this->slider->maximum()) {
        changedValue = this->slider->maximum();
    }
    this->sliderValue = changedValue;

    TickData *debugTickData = this->ticksDataStore[changedValue];
    DrawingData *debugDrawingData = new DrawingData(debugTickData);
    if (debugTickData == nullptr) {
        finishTick(debugTickData, debugDrawingData);
        return;
    }

    debugMutex.lock();
    prepareTickData(debugTickData, debugDrawingData);
    auto &context = debugTickData->getContext();
    auto &player = context.getPlayer();
    model::Move move;
    *this->debugStrategy = debugTickData->getMyStrategy();//copy old state
    this->debugStrategy->move(player, context.getWorld(), game, move);

    MainWindow::getInstance()->updateCalculatedStrategyDrawDataTick(*this->debugStrategy, debugTickData,
                                                                    debugDrawingData, move);
    finishTick(debugTickData, debugDrawingData);
    debugMutex.unlock();
}

int MainWindow::keyboardModifiersSliderShift(const Qt::KeyboardModifiers &modifiers) {
    if (modifiers.testFlag(Qt::KeyboardModifier::ShiftModifier)) {
        return 100;
    }
    if (modifiers.testFlag(Qt::KeyboardModifier::ControlModifier)) {
        return 10;
    }
    return 1;
}

void MainWindow::runRepaint() {
    this->drawingFpsMeasurer.putTimestamp();
    this->slider->blockSignals(true);
    this->slider->setDisabled(true);
    this->slider->setValue(this->sliderValue);
    this->slider->setRange(this->sliderRangeStart, this->sliderRangeEnd);
    this->slider->setDisabled(false);
    this->slider->blockSignals(false);

    this->drawingDataStorage->selectAndCleanDrawingData();
    auto drawingData = this->drawingDataStorage->getCurrentDrawingData();
    int tickNom = slider->value();
    if (drawingData != nullptr && drawingData->getTickData() != nullptr) {
        tickNom = drawingData->getTickData()->getContext().getWorld().getTickIndex();
    }
    if (tickNom != this->lastPaintedTick) {
        updatePaintData(tickNom, drawingData);
        this->drawing->resetUpdated();
        this->textBrowser->resetUpdated();
        this->drawing->updateHelperData(this->coordDrawingHelper);

        this->centralWidget()->repaint();
        if (!this->drawing->isUpdated()) {
            std::cout << "force drawing repaint" << std::endl;
            this->drawing->repaint();
        }
        if (!this->textBrowser->isUpdated()) {
            std::cout << "force textBrowser repaint" << std::endl;
            this->textBrowser->repaint();
        }
    } else {
        updatePaintData(tickNom, drawingData);
        this->textBrowser->repaint();
    }
    qApp->processEvents(QEventLoop::ExcludeUserInputEvents);
    currentFrameTime += chronoMillisToNextDraw;
    std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
    int timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(currentFrameTime - current).count();
    if (timeDiff < minWaitBetweenDrawings) {
        timeDiff = minWaitBetweenDrawings;
//        currentFrameTime = current + 1ms;
        currentFrameTime = std::chrono::steady_clock::now() + std::chrono::milliseconds(minWaitBetweenDrawings);
    }
    QTimer::singleShot(timeDiff, Qt::TimerType::CoarseTimer, this, SLOT(runRepaint()));
    this->drawingFpsMeasurer.putTime();
}

void MainWindow::wheelEvent(QWheelEvent *event) {
    if (this->drawing->geometry().contains(event->pos())) {
        const QRect &rect = this->drawing->geometry();
        double rollX = (event->x() - rect.x()) / (rect.width() * 1.);
        double rollY = (event->y() - rect.y()) / (rect.height() * 1.);
        if (event->delta() > 0) {
            this->coordDrawingHelper->zoomIn(rollX, rollY);
        } else {
            this->coordDrawingHelper->zoomOut(rollX, rollY);
        }
        this->lastPaintedTick = -2;
    } else {
        QWidget::wheelEvent(event);
    }
}

void MainWindow::mousePressEvent(QMouseEvent *event) {
    if (event->button() == Qt::MiddleButton) {
        middleMousePressPoint = event->pos();
        return;
    } else if (event->button() == Qt::LeftButton) {
        QPoint clickPoint = event->pos();
        clickPoint.setX(clickPoint.x() - this->drawing->x());
        clickPoint.setY(clickPoint.y() - this->drawing->y());
        QPointF point = this->coordDrawingHelper->screenToOrig(clickPoint);
        DrawingData *drawingData = this->drawingDataStorage->getCurrentDrawingData();
        long long nearestVehicleId = -1;
        double minDistance = 1e100;
        for (auto &unit : drawingData->getTickData()->getMyStrategy().myVehicles) {
            double dX = unit.second.position.x - point.x();
            double dY = unit.second.position.y - point.y();
            dX = dX * dX + dY * dY;
            if (dX < minDistance) {
                minDistance = dX;
                nearestVehicleId = unit.first;
            }
        }
        for (auto &unit : drawingData->getTickData()->getMyStrategy().enemyVehicles) {
            double dX = unit.second.position.x - point.x();
            double dY = unit.second.position.y - point.y();
            dX = dX * dX + dY * dY;
            if (dX < minDistance) {
                minDistance = dX;
                nearestVehicleId = unit.first;
            }
        }
        if (minDistance <= 4) {
            this->descriptionUnitId = nearestVehicleId;
        } else {
            this->descriptionUnitId = -1;
        }
        fullRedrawFrame();
        return;
    }
    QWidget::mousePressEvent(event);
}

void MainWindow::mouseReleaseEvent(QMouseEvent *event) {
    if (event->button() == Qt::MiddleButton) {
        middleMousePressPoint = QPoint(-1000, -1000);
    }
    QWidget::mouseReleaseEvent(event);
}

void MainWindow::mouseMoveEvent(QMouseEvent *event) {
    if (middleMousePressPoint.x() != -1000 || middleMousePressPoint.y() != -1000) {//map move
        QPoint pt = event->pos();
        QPoint diff = pt - middleMousePressPoint;
        this->coordDrawingHelper->scroll(diff);
        middleMousePressPoint = pt;
        this->lastPaintedTick = -2;
    }

    QWidget::mouseMoveEvent(event);
}

void MainWindow::prepareTickData(const model::PlayerContext &context, const MyStrategy &strategy) {
    this->currentTickData = new TickData(context, strategy);
    this->strategyDrawingData = new DrawingData(this->currentTickData);

    prepareTickData(this->currentTickData, this->strategyDrawingData);
    strategyFpsMeasurer.putTimestamp();
}

void MainWindow::updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy, const model::Move &move) {
    updateCalculatedStrategyDrawDataTick(strategy, this->currentTickData, this->strategyDrawingData, move);
}

void MainWindow::finishTick() {
    strategyFpsMeasurer.putTime();
    while (this->currentTickData->getContext().getWorld().getTickIndex() != this->ticksDataStore.size()) {
        this->ticksDataStore.push_back(nullptr);
    }
    this->ticksDataStore.push_back(this->currentTickData);

    this->sliderRangeStart = 0;
    this->sliderRangeEnd = ticksDataStore.size() - 1;

    if (drawLast) {
        this->sliderValue = ticksDataStore.size() - 1;
        finishTick(this->currentTickData, this->strategyDrawingData);
    } else {
        delete this->strategyDrawingData;
    }
}

void MainWindow::readyToNextFrame() {
    int designedFps = framesPerSecond[selectedSimulationSpeed];
    do {
        while (designedFps == 0) {
            std::this_thread::sleep_for(10ms);
            if (nextSim) {
                nextSim = false;
                return;
            }
            designedFps = framesPerSecond[selectedSimulationSpeed];
        }
        int millistToNextDraw = 1000 / designedFps;
        lastFrameSimulated += std::chrono::milliseconds(millistToNextDraw);
        std::chrono::steady_clock::time_point current = std::chrono::steady_clock::now();
        auto timeDiff = std::chrono::duration_cast<std::chrono::milliseconds>(lastFrameSimulated - current);
        if (timeDiff.count() > 0) {
            std::this_thread::sleep_for(timeDiff);
            designedFps = framesPerSecond[selectedSimulationSpeed];
        } else {
            lastFrameSimulated = current;
        }
    } while (designedFps == 0);
}


void MainWindow::prepareTickData(TickData *const tickData, DrawingData *const drawingData) {
    if (drawBackground) {
        double columnWidth = game.getWorldWidth() / game.getTerrainWeatherMapColumnCount();
        double rowHeight = game.getWorldHeight() / game.getTerrainWeatherMapRowCount();
        int x = 0;
//            TERRAIN_PLAIN = 0,
//            TERRAIN_SWAMP = 1,
//            TERRAIN_FOREST = 2,

        for (const auto &terrainRow : tickData->getContext().getWorld().getTerrainByCellXY()) {
            int y = 0;
            for (const auto &terrain : terrainRow) {
                drawingData->addFigure(std::make_unique<TexturePainter>(x * columnWidth + columnWidth / 2.,
                                                                        y * rowHeight + columnWidth / 2.,
                                                                        (TextureType) (int(terrain) +
                                                                                       TextureType::Grass),
                                                                        rowHeight,
                                                                        -10000));
                ++y;
            }
            ++x;
        }

        x = 0;
////        WEATHER_CLEAR = 0,
////        WEATHER_CLOUD = 1,
////        WEATHER_RAIN = 2,

        for (const auto &weatherRow : tickData->getContext().getWorld().getWeatherByCellXY()) {
            int y = 0;
            for (const auto &weather : weatherRow) {
                if (weather != model::WeatherType::CLEAR) {
                    drawingData->addFigure(std::make_unique<TexturePainter>(x * columnWidth + columnWidth / 2.,
                                                                            y * rowHeight + columnWidth / 2.,
                                                                            (TextureType) (int(weather) +
                                                                                           TextureType::Forest), //last not weather due clear skip
                                                                            rowHeight,
                                                                            -9999));
                }
                ++y;
            }
            ++x;
        }
        { // lines
            QPen pen = QPen(QColor(0, 0, 0, 50));
            for (int i = 1; i != 32; ++i) {
                drawingData->addFigure(std::make_unique<Line>(i * columnWidth,
                                                              0,
                                                              i * columnWidth,
                                                              game.getWorldHeight(),
                                                              pen,
                                                              -9998));
            }
            for (int j = 1; j != 32; ++j) {
                drawingData->addFigure(std::make_unique<Line>(0,
                                                              j * rowHeight,
                                                              game.getWorldWidth(),
                                                              j * rowHeight,
                                                              pen,
                                                              -9998));
            }
        }
    }
}

void MainWindow::updateCalculatedStrategyDrawDataTick(const MyStrategy &strategy,
                                                      TickData *const tickData,
                                                      DrawingData *const drawingData,
                                                      const model::Move &move) {
    {
        QBrush brush = QBrush(QColor(200, 200, 255, 255));
        QBrush brushSelected = QBrush(QColor(150, 150, 200, 255));

        for (auto &item: strategy.myVehicles) {
            //Circle(double x, double y, int radius, QBrush color);
            auto &vehicle = item.second;
            int priority = (((int) CalcUtil::isAerial(vehicle)) * 4);
            if (this->descriptionUnitId != item.first) {
                drawingData->addFigure(std::make_unique<Circle>(vehicle.position.x,
                                                                vehicle.position.y,
                                                                game.getVehicleRadius(),
                                                                vehicle.selected ? brushSelected : brush,
                                                                priority));
            } else {
                drawingData->addFigure(std::make_unique<Circle>(vehicle.position.x,
                                                                vehicle.position.y,
                                                                game.getVehicleRadius(),
                                                                vehicle.selected ? brushSelected : brush,
                                                                QPen(Qt::white),
                                                                priority));
            }
            drawingData->addFigure(std::make_unique<TexturePainter>(vehicle.position.x,
                                                                    vehicle.position.y,
                                                                    (TextureType) vehicle.vType,
                                                                    game.getVehicleRadius() * 1.6,
                                                                    priority + 1));

            drawingData->addFigure(std::make_unique<StatusBar>(vehicle.position.x,
                                                               vehicle.position.y,
                                                               game.getVehicleRadius() * 2,
                                                               vehicle.durability * 1.f /
                                                               game.getFighterDurability(),// at least now it's ok....
                                                               1.f - vehicle.cooldownTicks * 1.f /
                                                                     game.getFighterAttackCooldownTicks(),
                                                               priority + 2));
        }
    }

    {
        QBrush brush = QBrush(QColor(255, 0, 0, 255));
        for (auto &item: strategy.enemyVehicles) {
            //Circle(double x, double y, int radius, QBrush color);
            auto &vehicle = item.second;
            int priority = (((int) CalcUtil::isAerial(vehicle)) * 4) - 2;
            if (this->descriptionUnitId != item.first) {
                drawingData->addFigure(std::make_unique<Circle>(vehicle.position.x,
                                                                vehicle.position.y,
                                                                game.getVehicleRadius(),
                                                                brush,
                                                                priority));
            } else {
                drawingData->addFigure(std::make_unique<Circle>(vehicle.position.x,
                                                                vehicle.position.y,
                                                                game.getVehicleRadius(),
                                                                brush,
                                                                QPen(Qt::white),
                                                                priority));
            }
            drawingData->addFigure(std::make_unique<TexturePainter>(vehicle.position.x,
                                                                    vehicle.position.y,
                                                                    (TextureType) vehicle.vType,
                                                                    game.getVehicleRadius() * 1.6,
                                                                    priority + 1));

            drawingData->addFigure(std::make_unique<StatusBar>(vehicle.position.x,
                                                               vehicle.position.y,
                                                               game.getVehicleRadius() * 2,
                                                               vehicle.durability * 1.f /
                                                               game.getFighterDurability(),// at least now it's ok....
                                                               1.f - vehicle.cooldownTicks * 1.f /
                                                                     game.getFighterAttackCooldownTicks(),
                                                               priority + 2));
        }
    }

    {
        const MyVehicle *vehicle = nullptr;
        long long vehicleId;
        auto iterFound = strategy.enemyVehicles.find(this->descriptionUnitId);
        if (iterFound != strategy.enemyVehicles.end()) {
            vehicle = &iterFound->second;
            vehicleId = iterFound->first;
        } else {
            iterFound = strategy.myVehicles.find(this->descriptionUnitId);
            if (iterFound != strategy.myVehicles.end()) {
                vehicle = &iterFound->second;
                vehicleId = iterFound->first;
            }
        }
        if (vehicle != nullptr) {
            if (vehicle->expectedTicksForMovement == 0) {
                drawingData->putLine(format("type: %s (%d)\n(%.2f;%.2f)",
                                            vehicleTypes[int(vehicle->vType)],
                                            vehicleId,
                                            vehicle->position.x,
                                            vehicle->position.y), 3);
            } else {
                drawingData->putLine(format("type: %s (%d)\n(%.2f;%.2f)->(%.2f;%.2f)~%d",
                                            vehicleTypes[int(vehicle->vType)],
                                            vehicleId,
                                            vehicle->position.x,
                                            vehicle->position.y,
                                            vehicle->destination.x,
                                            vehicle->destination.y,
                                            vehicle->expectedTicksForMovement), 3);
            }
        } else {
            drawingData->putLine("no units selected", 3);
        }
    }

    switch (move.getAction()) {
        case model::ActionType::_UNKNOWN_:
            drawingData->putLine("Action: not set", 2);
            break;
        case model::ActionType::NONE:
            drawingData->putLine("Action: NONE", 2);
            break;
        case model::ActionType::CLEAR_AND_SELECT:
            drawingData->putLine(format("Action: CLEAR_AND_SELECT\n[(%.2f;%.2f),(%.2f;%.2f)]",
                                        move.getLeft(),
                                        move.getTop(),
                                        move.getRight(),
                                        move.getBottom()), 2);
            break;
        case model::ActionType::ADD_TO_SELECTION:
            drawingData->putLine("Action: ADD_TO_SELECTION", 2);
            break;
        case model::ActionType::DESELECT:
            drawingData->putLine("Action: DESELECT", 2);
            break;
        case model::ActionType::ASSIGN:
            drawingData->putLine(format("Action: ASSIGN %d", move.getGroup()), 2);
            break;
        case model::ActionType::DISMISS:
            drawingData->putLine("Action: DISMISS", 2);
            break;
        case model::ActionType::DISBAND:
            drawingData->putLine("Action: DISBAND", 2);
            break;
        case model::ActionType::MOVE:
            drawingData->putLine(format("Action: MOVE\n(%.2f;%.2f)", move.getX(), move.getY()), 2);
            break;
        case model::ActionType::SCALE:
            drawingData->putLine(
                    format("Action: SCALE\n(%.2f;%.2f), %.2f", move.getX(), move.getY(), move.getMaxSpeed()), 2);
            break;
        case model::ActionType::ROTATE:
            drawingData->putLine(format("Action: ROTATE\n[(%.2f;%.2f),%.2f/%.2f]",
                                        move.getX(),
                                        move.getY(),
                                        move.getMaxAngularSpeed() / M_PI * 180.,
                                        move.getAngle() / M_PI * 180.), 2);
            break;
        case model::ActionType::SETUP_VEHICLE_PRODUCTION:
            drawingData->putLine("Action: SETUP_VEHICLE_PRODUCTION", 2);
            break;
        case model::ActionType::TACTICAL_NUCLEAR_STRIKE:
            drawingData->putLine(format("Action: TACTICAL_NUCLEAR_STRIKE (%.2f;%.2f) by %d",
                                        move.getX(),
                                        move.getY(),
                                        move.getVehicleId()), 2);
            break;
        default:
            drawingData->putLine("Action: WAT", 2);
            break;
    }

    drawingData->putLine(format("Cd: %d, availableCount: %d",
                                tickData->getContext().getPlayer().getRemainingActionCooldownTicks(),
                                CalcUtil::visAvaliableActionsCount(strategy)), 4);

    //calculate vehicles by type
    std::vector<int> myCounts = CalcUtil::getCounts(strategy.myVehicles);
    std::vector<int> enCounts = CalcUtil::getCounts(strategy.enemyVehicles);
    drawingData->putLine(format("Balance:\nTANK: %d-%d\nAA: %d-%d\nENG: %d-%d\nJET: %d-%d\nHELI: %d-%d",
                                myCounts[int(model::VehicleType::TANK)],
                                enCounts[int(model::VehicleType::TANK)],
                                myCounts[int(model::VehicleType::IFV)],
                                enCounts[int(model::VehicleType::IFV)],
                                myCounts[int(model::VehicleType::ARRV)],
                                enCounts[int(model::VehicleType::ARRV)],
                                myCounts[int(model::VehicleType::FIGHTER)],
                                enCounts[int(model::VehicleType::FIGHTER)],
                                myCounts[int(model::VehicleType::HELICOPTER)],
                                enCounts[int(model::VehicleType::HELICOPTER)]), 5);

    if (drawBlocks) {
        for (auto &block : strategy.blocks) {
            if (!(drawBlocks & 1) && block.ground) {
                continue;
            }
            if (!(drawBlocks & 2) && !block.ground) {
                continue;
            }
            QPen pen = QPen(block.ground ? QColor(0, 0, 0, 255) : QColor(255, 0, 0, 255));
            //Line(double startX, double startY, double endX, double endY, const QPen &pen);
            drawingData->addFigure(std::make_unique<Line>(block.currentPosMin.x,
                                                          block.currentPosMin.y,
                                                          block.dest.x,
                                                          block.dest.y,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Line>(block.currentPosMin.x - 5,
                                                          block.currentPosMin.y - 5,
                                                          block.currentPosMin.x + 5,
                                                          block.currentPosMin.y + 5,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Line>(block.currentPosMin.x + 5,
                                                          block.currentPosMin.y - 5,
                                                          block.currentPosMin.x - 5,
                                                          block.currentPosMin.y + 5,
                                                          pen,
                                                          11000.));

            drawingData->addFigure(std::make_unique<Line>(block.dest.x - 5,
                                                          block.dest.y - 5,
                                                          block.dest.x + 5,
                                                          block.dest.y + 5,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Line>(block.dest.x + 5,
                                                          block.dest.y - 5,
                                                          block.dest.x - 5,
                                                          block.dest.y + 5,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Line>(block.currentPosMax.x - 5,
                                                          block.currentPosMax.y - 5,
                                                          block.currentPosMax.x + 5,
                                                          block.currentPosMax.y + 5,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Line>(block.currentPosMax.x + 5,
                                                          block.currentPosMax.y - 5,
                                                          block.currentPosMax.x - 5,
                                                          block.currentPosMax.y + 5,
                                                          pen,
                                                          11000.));
            drawingData->addFigure(std::make_unique<Circle>(block.currentPosMax.x,
                                                            block.currentPosMax.y,
                                                            block.radius,
                                                            pen,
                                                            11000.));
            drawingData->addFigure(std::make_unique<Circle>(block.dest.x,
                                                            block.dest.y,
                                                            block.radius,
                                                            pen,
                                                            11000.));
            drawingData->addFigure(std::make_unique<Circle>(block.currentPosMin.x,
                                                            block.currentPosMin.y,
                                                            block.radius,
//                                                            brush,
                                                            pen,
                                                            11000.));
        }
    }
}

void MainWindow::finishTick(TickData *const tickData, DrawingData *const drawingData) {
    if (drawJetInfluence) {
        drawField(drawingData, VarStorage::jetInfluenceField, drawJetInfluence);
    }
    if (drawHeliInfluence) {
        drawField(drawingData, VarStorage::heliInfluenceField, drawHeliInfluence);
    }
//    if (drawTankInfluence) {
//        drawField(drawingData, VarStorage::tankInfluenceField);
//    }
//    if (drawIFVInfluence) {
//        drawField(drawingData, VarStorage::IFVinfluenceField);
//    }
//    if (drawARRVInfluence) {
//        drawField(drawingData, VarStorage::ARRVInfluenceField);
//    }

//    auto &myStrategy = tickData->getMyStrategy();
//    QColor fogColor = QColor(0, 0, 0, 100);
//    for (int i = 0; i != this->coordDrawingHelper->getWidth(); ++i) {
//        bool visible = false;
//        for (int j = 0; j != this->coordDrawingHelper->getHeight(); ++j) {
//            QPointF point = this->coordDrawingHelper->screenToOrig(QPoint(i, j));
//            for (auto &unit : myStrategy.myVehicles) {
//                double val = unit.second.position.x - point.x();
//                val *= val;
//                double visionRange = CalcUtil::getVisionRange(unit.second.vType);
//                visionRange *= visionRange;
//                if (val > visionRange) {
//                    continue;
//                }
//                double val2 = unit.second.position.y - point.y();
//                val2 *= val2;
//                if (val2 > visionRange) {
//                    continue;
//                }
////                auto terrain = tickData->getContext().getWorld().getTerrainByCellXY()[int(unit.second.position.x / 32.)][int(unit.second.position.y / 32.)];
//                if (val2 + val < visionRange) {
//                    visible = true;
//                    break;
//                }
//            }
//            if (!visible) {
//                drawingData->addFigure(std::make_unique<Pixel>(i, j, fogColor, 10000));
//            }
//        }
//    }


    this->drawingDataStorage->updateDrawingData(drawingData);
}

void MainWindow::fullRedrawFrame() {
    if (drawLast) {
        return;
    }
    int lastPaintedTick = this->lastPaintedTick;
    this->lastPaintedTick = -2;
    changeDrawingTick(lastPaintedTick);
}

void MainWindow::drawField(DrawingData *const drawingData,
                           double (&paintInfluenceField)[Constants::fieldsCount][Constants::influenceMapSize][Constants::influenceMapSize],
                           int type) {
    double (&influenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = paintInfluenceField[type - 1];

    double tileWidth = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
    double tileHeight = CalcUtil::game.getWorldHeight() / Constants::influenceMapSize;
    double min = std::numeric_limits<double>::max();
    double max = std::numeric_limits<double>::min();
    for (int i = 0; i != Constants::influenceMapSize; ++i) {
        for (int j = 0; j != Constants::influenceMapSize; ++j) {
            if (min > influenceField[i][j]) {
                min = influenceField[i][j];
            }
            if (max < influenceField[i][j]) {
                max = influenceField[i][j];
            }
        }
    }
    LOG_DEBUG("min:% max:%", min, max);
    QColor argbStart = QColor(255, 0, 0, 100);
    QColor argbEnd = QColor(0, 255, 0, 100);;
    for (int i = 0; i != Constants::influenceMapSize; ++i) {
        for (int j = 0; j != Constants::influenceMapSize; ++j) {
            drawingData->addFigure(std::make_unique<Rect>(tileWidth * i,
                                                          tileHeight * j,
                                                          tileWidth,
                                                          tileHeight,
                                                          MidColorUtil::getColor(argbStart,
                                                                                 argbEnd,
                                                                                 min,
                                                                                 max,
                                                                                 influenceField[i][j]),
                                                          10000));

        }
    }
}
