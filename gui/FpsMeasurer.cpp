//
// Created by dragoon on 03.11.17.
//

#include "FpsMeasurer.h"

FpsMeasurer::FpsMeasurer(){
    prevTime = std::chrono::steady_clock::now();
}

void FpsMeasurer::putTimestamp() {
    prevTime = std::chrono::steady_clock::now();
}

void FpsMeasurer::putTime() {
    auto now = std::chrono::steady_clock::now();
    long times = std::chrono::duration_cast<std::chrono::microseconds>(now - prevTime).count();
    timings.push_back(times);
    timingsSum += times;
    while (timingsSum > 1000000L && timings.size() > 5) {
        timingsSum -= timings.front();
        timings.pop_front();
    }
}

double FpsMeasurer::getFps() {
    return (timings.size() * 1e6) / timingsSum;
}
