//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_TICKDATA_H
#define CPP_CGDK_TICKDATA_H


#include <model/PlayerContext.h>
#include <MyStrategy.h>

class TickData {
public:
    TickData(const model::PlayerContext &context, const MyStrategy &myStrategy);

private:
    model::PlayerContext context;
    MyStrategy strategy;

public:
    const model::PlayerContext &getContext() const;

    const MyStrategy &getMyStrategy() const;
};


#endif //CPP_CGDK_TICKDATA_H
