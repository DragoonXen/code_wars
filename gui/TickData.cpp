//
// Created by dragoon on 10/21/17.
//

#include "TickData.h"

TickData::TickData(const model::PlayerContext &context, const MyStrategy &myStrategy) : context(context), strategy(myStrategy) {}

const model::PlayerContext &TickData::getContext() const {
    return context;
}

const MyStrategy &TickData::getMyStrategy() const {
    return this->strategy;
}
