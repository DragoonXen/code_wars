//
// Created by dragoon on 03.11.17.
//

#ifndef CPP_CGDK_FPSMEASURER_H
#define CPP_CGDK_FPSMEASURER_H


#include <chrono>
#include <list>

class FpsMeasurer {

private:
    std::chrono::steady_clock::time_point prevTime;
    std::list<long> timings;
    long timingsSum = 0;

public:
    FpsMeasurer();

    void putTimestamp();

    void putTime();

    double getFps();
};


#endif //CPP_CGDK_FPSMEASURER_H
