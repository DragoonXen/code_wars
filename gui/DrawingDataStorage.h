//
// Created by dragoon on 27.10.17.
//

#ifndef CPP_CGDK_DRAWINGDATASTORAGE_H
#define CPP_CGDK_DRAWINGDATASTORAGE_H


#include <mutex>
#include "DrawingData.h"

class DrawingDataStorage {

    struct DrawingItem {
        DrawingData *drawingData;
        DrawingItem *nextItem;

        DrawingItem(DrawingData *drawingData, DrawingItem *item);
    };

    static std::mutex mutex;

public:
    void updateDrawingData(DrawingData *strategyDrawingData);

    void selectAndCleanDrawingData();

    DrawingData *getCurrentDrawingData() const;

private:
    DrawingItem *currentDrawingDataItem = nullptr;
    DrawingData *currentDrawingData = nullptr;
};

#endif //CPP_CGDK_DRAWINGDATASTORAGE_H
