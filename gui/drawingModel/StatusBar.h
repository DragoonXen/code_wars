//
// Created by dragoon on 10/22/17.
//

#ifndef CPP_CGDK_HEALTHBAR_H
#define CPP_CGDK_HEALTHBAR_H


#include "Figure.h"

class StatusBar : public Figure {

private:
    static constexpr double healthBarHeight = .5;
    double cX, cY;
    float lifePercentile;
    float reloadPercentile;
    int size;
public:
    StatusBar(double cX, double cY, int size, float lifePercentile, float reloadPercentile = 1.f);

    StatusBar(double cX, double cY, int size, float lifePercentile, int priority);

    StatusBar(double cX, double cY, int size, float lifePercentile, float reloadPercentile, int priority);

    ~StatusBar() override;

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;
};


#endif //CPP_CGDK_HEALTHBAR_H
