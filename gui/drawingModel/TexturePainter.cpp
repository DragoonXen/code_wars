//
// Created by dragoon on 24.10.17.
//

#include <iostream>
#include "TexturePainter.h"

std::vector<QImage *> TexturePainter::pixMaps;
std::vector<QImage *> TexturePainter::scaled;

void TexturePainter::initTextures() {
    if (!TexturePainter::pixMaps.empty()) {
        return;
    }
    loadImage("../textures/brem.png");
    loadImage("../textures/airplane.png");
    loadImage("../textures/heli.jpg");
    loadImage("../textures/PVO.jpg");
    loadImage("../textures/tank.jpg");

    loadImage("../textures/grass.png", false);
    loadImage("../textures/swamp.png", false);
    loadImage("../textures/forest.png", false);

    loadImage("../textures/clouds.png", false);
    loadImage("../textures/rain.png", false);
}

void TexturePainter::loadImage(QString imagePath, bool postProcessing) {
    QImage *image = new QImage();
    image->load(imagePath);
//
    if (image->size().width() != image->size().height()) {
        std::cout << "loaded image has incorrect dimentions: " << image->size().width() << ' ' << image->size().height()
                  << std::endl;
    }
    if (image->size().width() == 0) {
        std::cout << "no image loaded" << std::endl;
    }
    if (postProcessing) {
        QImage *alphaImage = new QImage(image->size(), QImage::Format_ARGB32);

        QRgb transparent = qRgba(255, 255, 255, 0);
        QRgb black = qRgba(0, 0, 0, 255);
        for (int i = 0; i != image->width(); ++i) {
            for (int j = 0; j != image->height(); ++j) {
                QRgb rgb = image->pixel(i, j);
                if (qIsGray(rgb) && (qRed(rgb) > 200 || qAlpha(rgb) < 128)) { // skip not black
                    alphaImage->setPixel(i, j, transparent);
                } else {
                    alphaImage->setPixel(i, j, black);
                }
            }
        }
        delete image;

        TexturePainter::pixMaps.push_back(alphaImage);
        TexturePainter::scaled.push_back(alphaImage);
    } else {
        TexturePainter::pixMaps.push_back(image);
        TexturePainter::scaled.push_back(image);
    }
}

QImage *TexturePainter::getScaledImage(const int textureType, const int size) {
    if (TexturePainter::scaled[textureType]->size().width() == size) {
        return TexturePainter::scaled[textureType];
    }
    if (TexturePainter::scaled[textureType] != TexturePainter::pixMaps[textureType]) {
        delete TexturePainter::scaled[textureType];
    }
    std::cout << "scaling " << std::endl;

    QImage *scaledImage = new QImage(size, size, QImage::Format_ARGB32);
    *scaledImage = TexturePainter::pixMaps[textureType]->scaled(size, size, Qt::KeepAspectRatio,
                                                                Qt::SmoothTransformation);
    TexturePainter::scaled[textureType] = scaledImage;

    return scaledImage;
}

void TexturePainter::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {
    if (textureType < 0 || textureType >= TexturePainter::pixMaps.size()) {
        std::cout << "wrong texture type " << textureType << std::endl;
        return;
    }
    double textureSize = drawingHelper.fixSize(size);
    QImage *scaledImage = getScaledImage(textureType, (int) textureSize); //round down is ok enought
    double sX = drawingHelper.fixX(x) - textureSize / 2;
    double sY = drawingHelper.fixY(y) - textureSize / 2;
    painter.drawImage(QRectF(sX, sY, textureSize, textureSize), *scaledImage);
}

TexturePainter::TexturePainter(double x, double y, TextureType textureType, int size, int priority)
        : Figure(priority), size(size), x(x), y(y), textureType(textureType) {}
