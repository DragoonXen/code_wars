//
// Created by dragoon on 13.11.17.
//

#include "Pixel.h"

Pixel::Pixel(int x, int y, const QColor &color) : x(x), y(y), color(color) {}

Pixel::Pixel(int x, int y, const QColor &color, int priority) : Figure(priority), x(x), y(y), color(color) {}

Pixel::~Pixel() {

}

void Pixel::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {
    painter.setPen(this->color);
    painter.drawPoint(this->x, this->y);
}
