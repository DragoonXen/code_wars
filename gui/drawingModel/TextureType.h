//
// Created by dragoon on 25.10.17.
//

#ifndef CPP_CGDK_TEXTURETYPE_H
#define CPP_CGDK_TEXTURETYPE_H

enum TextureType {
    BREM, Plane, Heli, BMP, Tank,

    Grass, Swamp, Forest,

    Clouds, Rain
};

#endif //CPP_CGDK_TEXTURETYPE_H
