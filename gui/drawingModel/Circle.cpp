//
// Created by dragoon on 10/21/17.
//

#include "Circle.h"

Circle::Circle(double x, double y, double radius, QBrush color) : x(x), y(y), radius(radius), color(color), borderColor(Qt::NoPen) {
}

Circle::Circle(double x, double y, double radius, QPen borderColor) : x(x), y(y), radius(radius), color(Qt::NoBrush), borderColor(borderColor) {
}

Circle::Circle(double x, double y, double radius, QBrush color, QPen borderColor) : x(x), y(y), radius(radius), color(color), borderColor(borderColor) {
}

Circle::Circle(double x, double y, double radius, QBrush color, int priority) : Figure(priority), x(x), y(y), radius(radius), color(color),
                                                                             borderColor(Qt::NoPen) {
}

Circle::Circle(double x, double y, double radius, QPen borderColor, int priority) : Figure(priority),
                                                                                 x(x),
                                                                                 y(y),
                                                                                 radius(radius),
                                                                                 color(Qt::NoBrush),
                                                                                 borderColor(borderColor) {
}

Circle::Circle(double x, double y, double radius, QBrush color, QPen borderColor, int priority) : Figure(priority),
                                                                                               x(x),
                                                                                               y(y),
                                                                                               radius(radius),
                                                                                               color(color),
                                                                                               borderColor(borderColor) {
}

void Circle::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {
    painter.setBrush(this->color);
    painter.setPen(this->borderColor);

    painter.drawEllipse(QPointF(drawingHelper.fixX(x),
                                drawingHelper.fixY(y)),
                        drawingHelper.fixSize(radius),
                        drawingHelper.fixSize(radius));
}

Circle::~Circle() {
}




