//
// Created by dragoon on 24.10.17.
//

#ifndef CPP_CGDK_TEXTUREPAINTER_H
#define CPP_CGDK_TEXTUREPAINTER_H


#include <QtGui/QPixmap>
#include "Figure.h"
#include "TextureType.h"

class TexturePainter : public Figure {

private:
    static std::vector<QImage *> pixMaps;
    static std::vector<QImage *> scaled;

    static void loadImage(QString imagePath, bool postProcessing = true);

    static QImage *getScaledImage(const int textureType, const int size);

public:
    static void initTextures();

    int size;

    double x, y;

    TextureType textureType;

    TexturePainter(double x, double y, TextureType textureType, int size, int priority = 0);

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;
};


#endif //CPP_CGDK_TEXTUREPAINTER_H
