//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_CIRCLE_H
#define CPP_CGDK_CIRCLE_H


#include "Figure.h"

class Circle : public Figure {

private:
    double x, y;
    double radius;
    QBrush color;
    QPen borderColor;

public:
    Circle(double x, double y, double radius, QBrush color);

    Circle(double x, double y, double radius, QPen borderColor);

    Circle(double x, double y, double radius, QBrush color, QPen borderColor);

    Circle(double x, double y, double radius, QBrush color, int priority);

    Circle(double x, double y, double radius, QPen borderColor, int priority);

    Circle(double x, double y, double radius, QBrush color, QPen borderColor, int priority);

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;

    ~Circle() override;
};


#endif //CPP_CGDK_CIRCLE_H
