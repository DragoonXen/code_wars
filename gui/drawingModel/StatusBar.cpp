//
// Created by dragoon on 10/22/17.
//

#include "StatusBar.h"

StatusBar::~StatusBar() {
}

void StatusBar::paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const {

    double upY = cY - size * 0.5 - healthBarHeight * 2.5;

    double leftX = cX - size * 0.5;

    bool paintCooldown = reloadPercentile < 1.f;

    painter.setPen(Qt::NoPen);
    painter.setBrush(Qt::black);
    if (drawingHelper.fixSize(healthBarHeight) > 2) {
        painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX) - 1,
                                         drawingHelper.fixY(upY) - 1,
                                         drawingHelper.fixSize(size) + 2,
                                         drawingHelper.fixSize(healthBarHeight * (paintCooldown ? 2 : 1)) + 2));
    } else {
        painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX),
                                         drawingHelper.fixY(upY),
                                         drawingHelper.fixSize(size),
                                         drawingHelper.fixSize(healthBarHeight * (paintCooldown ? 2 : 1))));
    }

    painter.setBrush(Qt::red);
    painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX),
                                     drawingHelper.fixY(upY),
                                     drawingHelper.fixSize(size * lifePercentile),
                                     drawingHelper.fixSize(healthBarHeight)));
    if (paintCooldown) {
        upY += healthBarHeight;
        painter.setBrush(Qt::darkGreen);

        painter.drawConvexPolygon(QRectF(drawingHelper.fixX(leftX),
                                         drawingHelper.fixY(upY),
                                         drawingHelper.fixSize(size * reloadPercentile),
                                         drawingHelper.fixSize(healthBarHeight)));
    }
}

StatusBar::StatusBar(double cX, double cY, int size, float lifePercentile, float reloadPercentile) : Figure(1), cX(cX),
                                                                                                     cY(cY),
                                                                                                     lifePercentile(
                                                                                                             lifePercentile),
                                                                                                     reloadPercentile(
                                                                                                             reloadPercentile),
                                                                                                     size(size) {}

StatusBar::StatusBar(double cX, double cY, int size, float lifePercentile, int priority) : Figure(priority), cX(cX),
                                                                                           cY(cY),
                                                                                           lifePercentile(
                                                                                                   lifePercentile),
                                                                                           size(size) {}

StatusBar::StatusBar(double cX, double cY, int size, float lifePercentile, float reloadPercentile, int priority)
        : Figure(priority), cX(cX), cY(cY), lifePercentile(lifePercentile), reloadPercentile(reloadPercentile),
          size(size) {}
