//
// Created by dragoon on 13.11.17.
//

#ifndef CPP_CGDK_PIXEL_H
#define CPP_CGDK_PIXEL_H


#include "Figure.h"

class Pixel : public Figure {

    int x, y;
    QColor color;
public:
    Pixel(int x, int y, const QColor &color);

    Pixel(int x, int y, const QColor &color, int priority);

    void paintFigure(QPainter &painter, const CoordDrawingHelper &drawingHelper) const override;

    ~Pixel() override;
};


#endif //CPP_CGDK_PIXEL_H
