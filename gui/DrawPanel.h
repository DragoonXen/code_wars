//
// Created by dragoon on 26.10.17.
//

#ifndef CPP_CGDK_DRAWPANEL_H
#define CPP_CGDK_DRAWPANEL_H


#include <QtWidgets/QGraphicsView>
#include "CoordDrawingHelper.h"
#include "DrawingData.h"
#include "DrawingDataStorage.h"
#include "ForceUpdatedWidget.h"

class DrawPanel : public ForceUpdatedWidget {

Q_OBJECT

private:
    CoordDrawingHelper *coordDrawingHelper = nullptr;
    DrawingDataStorage *drawingDataStorage;

protected:
    void paintEvent(QPaintEvent *event) override;

public:
    DrawPanel(QWidget *parent, DrawingDataStorage *drawingDataStorage);

    void updateHelperData(CoordDrawingHelper *coordDrawingHelper);

};


#endif //CPP_CGDK_DRAWPANEL_H
