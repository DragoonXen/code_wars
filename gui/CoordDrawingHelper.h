//
// Created by dragoon on 10/21/17.
//

#ifndef CPP_CGDK_COORDDRAWINGHELPER_H
#define CPP_CGDK_COORDDRAWINGHELPER_H


#include <QtCore/QPoint>
#include <QtCore/QRect>

class CoordDrawingHelper {

    static constexpr double maxZoom = 100.;
    static constexpr double zoomStep = 1.5;
    static constexpr double scrollSpeed = 0.05;
private:
    double startX, startY, endX, endY;

    double cX, cY;
    double pPC;
    double windowX, windowY;
    int width, height;

public:
    enum Direction {
        UP, LEFT, RIGHT, DOWN
    };

    CoordDrawingHelper(double startX, double startY, double endX, double endY, int width, int height);

    void resizeDrawingWindow(int newWidth, int newHeight);

    void zoomIn();

    void zoomIn(double dPointX, double dPointY);

    void zoomOut();

    void zoomOut(double dPointX, double dPointY);

    double fixX(double xCoord) const;

    double fixY(double yCoord) const;

    QPointF screenToOrig(const QPoint &pos) const;

    double fixSize(double size) const;

    void scroll(Direction direction);

    void scroll(QPoint direction);

    int getWidth() const;

    int getHeight() const;

private:
    void fitWindow();

};


#endif //CPP_CGDK_COORDDRAWINGHELPER_H
