#include "MyStrategy.h"
#include "CalcUtil.h"
#include "VarStorage.h"
#include "Piatnashki.h"

#include <cmath>
#include <cstdlib>
#include <iostream>
#include <algorithm>
#include <queue>
#include "SimpleLog.h"

#include "Action.h"
#include "I_JetInfluenceMapCalc.h"
#include "I_HeliInfluenceMapCalc.h"
#include "I_TankInfluenceMapCalc.h"
#include "I_IFVInfluenceMapCalc.h"
#include "I_ARRVInfluenceMapCalc.h"
#include "MoveDirection.h"

using namespace model;
using namespace std;

void MyStrategy::prepareUnitsList(const World &world) {
    auto myPlayerId = world.getMyPlayer().getId();
    for (auto &vehicle : world.getNewVehicles()) {
        if (vehicle.getPlayerId() == myPlayerId) {
            MyStrategy::myVehicles.insert(pair<long long, MyVehicle>(vehicle.getId(), MyVehicle(vehicle)));
            MyStrategy::myVehiclesIds.insert(vehicle.getId());
        } else {
            MyStrategy::enemyVehicles.insert(pair<long long, MyVehicle>(vehicle.getId(), MyVehicle(vehicle)));
        }
    }
    for (auto &vehicle : this->myVehicles) {
        vehicle.second.moved = false;
    }
    for (auto &vehicle : this->enemyVehicles) {
        vehicle.second.moved = false;
    }
    for (auto &vehicle : world.getVehicleUpdates()) {
        if (MyStrategy::myVehiclesIds.find(vehicle.getId()) != MyStrategy::myVehiclesIds.end()) {
            auto iter = MyStrategy::myVehicles.find(vehicle.getId());
            if (vehicle.getY() == 0) {
#ifndef WITH_VISUALIZER
                auto &storedVehicle = iter->second;
                for (auto &group : storedVehicle.groups) {
                    MyStrategy::groups[group].erase(
                            std::find(MyStrategy::groups[group].begin(),
                                      MyStrategy::groups[group].end(),
                                      &storedVehicle));
                }
#endif
                MyStrategy::myVehicles.erase(vehicle.getId());
                MyStrategy::myVehiclesIds.erase(vehicle.getId());
            } else {
                iter->second.updateMyValues(vehicle);
                auto &storedVehicle = iter->second;
                for (int idx = 0; idx != storedVehicle.groups.size(); ++idx) {
                    int group = storedVehicle.groups[idx];
                    auto foundIter = std::find(vehicle.getGroups().begin(), vehicle.getGroups().end(), group);
                    if (foundIter == vehicle.getGroups().end()) {// vehicle excluded from group
                        MyStrategy::groups[group].erase(
                                std::find(MyStrategy::groups[group].begin(),
                                          MyStrategy::groups[group].end(),
                                          &storedVehicle));

                        storedVehicle.groups[idx] = *storedVehicle.groups.rbegin();// replace with last element
                        storedVehicle.groups.pop_back();
                        --idx;//and repeat from last element idx again
                    }
                }
                for (int group : vehicle.getGroups()) {
                    auto foundIter = std::find(storedVehicle.groups.begin(), storedVehicle.groups.end(), group);
                    if (foundIter == storedVehicle.groups.end()) {// vehicle included from group
                        MyStrategy::groups[group].push_back(&storedVehicle);
                        storedVehicle.groups.push_back(group);
                    }
                }
            }
        } else {
            if (vehicle.getY() == 0) {
                MyStrategy::enemyVehicles.erase(vehicle.getId());
            } else {
                MyStrategy::enemyVehicles.find(vehicle.getId())->second.updateEnValues(vehicle);
            }
        }
    }
    int cnt = int(model::VehicleType::_COUNT_);
    for (int i = 0; i != cnt; ++i) {
        VarStorage::myVehiclesByType[i].clear();
        VarStorage::enemyVehiclesByType[i].clear();
    }

    VarStorage::selectedUnits.clear();
    VarStorage::myVehiclesVc.clear();
#ifdef WITH_VISUALIZER // need re-create all groups
    for (int i = 1; i != 101; ++i) {
        this->groups[i].clear();
    }
#endif
    for (auto &vehicle : myVehicles) {
        if (vehicle.second.selected) {
            VarStorage::selectedUnits.push_back(&vehicle.second);
        }
        VarStorage::myVehiclesVc.push_back(&vehicle.second);
        VarStorage::myVehiclesByType[int(vehicle.second.vType)].push_back(&vehicle.second);
#ifdef WITH_VISUALIZER // need re-create all groups
        for (auto &group : vehicle.second.groups) {
            this->groups[group].push_back(&vehicle.second);
        }
#endif
    }
    VarStorage::enemyVehiclesVc.clear();
    for (auto &vehicle : enemyVehicles) {
        VarStorage::enemyVehiclesVc.push_back(&vehicle.second);
        VarStorage::enemyVehiclesByType[int(vehicle.second.vType)].push_back(&vehicle.second);
    }
//    if (myVehicles.find(619) != myVehicles.end() && !myVehicles.find(619)->second.groups.empty()) {
//        LOG_ERROR("%", myVehicles.find(619)->second.groups[0])
//    }
}

vector<MyVehicle *> surroundedBy;

void MyStrategy::moveBlocks() {
    for (auto &block : blocks) {
        auto &vec = groups[block.groupId];
        bool moved = false;
        for (auto &vehicle : vec) {
            if (vehicle->moved) {
                moved = true;
                break;
            }
        }
        if (!moved) {
            continue;
        }
        //move block
        double sqrDistance = block.radius - CalcUtil::game.getVehicleRadius();
        sqrDistance *= sqrDistance;
        {
            surroundedBy.clear();
            for (auto &veh : vec) {
                if (veh->getSqrDistanceTo(block.currentPosMin) < sqrDistance + 1e-6) {
                    surroundedBy.push_back(veh);
                }
            }

            Point newMinPoint[] = {block.currentPosMin + block.onePointVector * block.maxSpeed,
                                   block.currentPosMin + block.onePointVector * ((block.maxSpeed + block.minSpeed) / 2),
                                   block.currentPosMin + block.onePointVector * block.minSpeed};

            for (int i = 0; i != 3; ++i) {
                bool ok = true;
                for (auto &veh : surroundedBy) {
                    if (veh->getSqrDistanceTo(newMinPoint[i]) > sqrDistance + 1e-6) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    block.currentPosMin = newMinPoint[i];
                    break;
                }
            }
            Point vectorToDest = block.dest - block.currentPosMin;
            if (vectorToDest.x * block.onePointVector.x < 0 || vectorToDest.y * block.onePointVector.y < 0) {
                block.currentPosMin = block.dest;
            }
        }
        if (!block.currentPosMax.equalsValues(block.dest)) {
            surroundedBy.clear();
            Point checkPoint = block.currentPosMax + block.onePointVector * block.maxSpeed;
            for (auto &veh : vec) {
                if (veh->getSqrDistanceTo(checkPoint) < sqrDistance + 1e-6) {
                    surroundedBy.push_back(veh);
                }
            }

            Point newMaxPoint[] = {block.currentPosMax,
                                   block.currentPosMax + block.onePointVector * block.minSpeed,
                                   block.currentPosMax +
                                   block.onePointVector * ((block.maxSpeed + block.minSpeed) / 2)};

            block.currentPosMax = checkPoint;
            for (int i = 0; i != 3; ++i) {
                bool ok = true;
                for (auto &veh : surroundedBy) {
                    if (veh->getSqrDistanceTo(newMaxPoint[i]) > sqrDistance + 1e-6) {
                        ok = false;
                        break;
                    }
                }
                if (ok) {
                    block.currentPosMax = newMaxPoint[i];
                    break;
                }
            }
            Point vectorToDest = block.dest - block.currentPosMax;
            if (vectorToDest.x * block.onePointVector.x < 0 || vectorToDest.y * block.onePointVector.y < 0) {
                block.currentPosMax = block.dest;
            }
        }

    }
}


inline double getMaxSpeed(model::VehicleType vType, const model::Game &game) {
    if (vType == model::VehicleType::TANK) {
        return game.getTankSpeed();
    }
    if (vType == model::VehicleType::IFV) {
        return game.getIfvSpeed();
    }
    if (vType == model::VehicleType::HELICOPTER) {
        return game.getHelicopterSpeed();
    }
    if (vType == model::VehicleType::FIGHTER) {
        return game.getFighterSpeed();
    }
    return game.getArrvSpeed();
}

inline double getVisionRange(model::VehicleType vType, const model::Game &game) {
    if (vType == model::VehicleType::TANK) {
        return game.getTankVisionRange();
    }
    if (vType == model::VehicleType::IFV) {
        return game.getIfvVisionRange();
    }
    if (vType == model::VehicleType::HELICOPTER) {
        return game.getHelicopterVisionRange();
    }
    if (vType == model::VehicleType::FIGHTER) {
        return game.getFighterVisionRange();
    }
    return game.getArrvVisionRange();
}

void MyStrategy::initConstants(const model::Game &game) {
    CalcUtil::game = game;
    for (int i = 0; i != int(model::VehicleType::_COUNT_); ++i) {
        CalcUtil::visionRange[i] = getVisionRange(model::VehicleType(i), game);
        CalcUtil::maxSpeed[i] = getMaxSpeed(model::VehicleType(i), game);
        CalcUtil::isGroundUnit[i] = !(model::VehicleType(i) == model::VehicleType::FIGHTER ||
                                      model::VehicleType(i) == model::VehicleType::HELICOPTER);
    }
    CalcUtil::initSpreadMatrix();
}

void MyStrategy::move(const Player &me, const World &world, const Game &game, Move &move) {
    if (world.getTickIndex() == 0) {
        initConstants(game);
    }
    VarStorage::currentMove = &move;
    VarStorage::currentPlayer = &me;
    for (int idx = 0; idx != world.getPlayers().size(); ++idx) {
        const Player &player = world.getPlayers()[idx];
        if (!player.isMe()) {
            VarStorage::opponentPlayer = &player;
            break;
        }
    }
    VarStorage::currentWorld = &world;
    LOG_DEBUG("debug tick no %", world.getTickIndex());
    if (!lastActionsTime.empty() &&
        lastActionsTime.front() + game.getActionDetectionInterval() == world.getTickIndex()) {
        lastActionsTime.erase(lastActionsTime.begin());
    }
    prepareUnitsList(world);
    moveBlocks();

    calculating(me, world, game, move); // here all logic

    if (move.getAction() != ActionType::_UNKNOWN_ && move.getAction() != ActionType::NONE) {
        lastActionsTime.push_back(world.getTickIndex());
    }

    if (move.getAction() == ActionType::MOVE) {
        Point vector = {move.getX(), move.getY()};
        for (auto &unit : CalcUtil::getSelectedUnits()) {
            unit->updateDestination(unit->position + vector);
        }
    } else if (move.getAction() == ActionType::SCALE) {
        Point centralPoint = {move.getX(), move.getY()};
        for (auto &unit : CalcUtil::getSelectedUnits()) {
            unit->updateDestination(centralPoint + (unit->position - centralPoint) * move.getFactor());
        }
    } else if (move.getAction() == ActionType::ROTATE) {
        Point centralPoint = {move.getX(), move.getY()};
        for (auto &unit : CalcUtil::getSelectedUnits()) {
            unit->updateDestination((unit->position - centralPoint).rotate(move.getAngle()) + centralPoint);
        }


//    } else if (move.getAction() == ActionType::ASSIGN) {
//        this->groupSelected = move.getGroup();
//    } else if (move.getAction() == ActionType::CLEAR_AND_SELECT) {
//        if (move.getGroup() > 0) {
//            this->groupSelected = move.getGroup();
//        }
    }

#ifdef LOG_INFO_ENABLED
    switch (move.getAction()) {
        case model::ActionType::CLEAR_AND_SELECT: LOG_INFO("Action: CLEAR_AND_SELECT\n[%;%],(%;%)]",
                                                           move.getLeft(),
                                                           move.getTop(),
                                                           move.getRight(),
                                                           move.getBottom());
            break;
        case model::ActionType::ADD_TO_SELECTION: LOG_INFO("Action: ADD_TO_SELECTION");
            break;
        case model::ActionType::DESELECT: LOG_INFO("Action: DESELECT");
            break;
        case model::ActionType::ASSIGN: LOG_INFO("Action: ASSIGN %", move.getGroup());
            break;
        case model::ActionType::DISMISS: LOG_INFO("Action: DISMISS");
            break;
        case model::ActionType::DISBAND: LOG_INFO("Action: DISBAND");
            break;
        case model::ActionType::MOVE: LOG_INFO("Action: MOVE\n(%;%)", move.getX(), move.getY());
            break;
        case model::ActionType::SCALE: LOG_INFO(
                "Action: SCALE\n(%;%), %", move.getX(), move.getY(), move.getMaxSpeed());
            break;
        case model::ActionType::ROTATE: LOG_INFO("Action: ROTATE\n[(%;%),%/%]",
                                                 move.getX(),
                                                 move.getY(),
                                                 move.getMaxAngularSpeed() / M_PI * 180.,
                                                 move.getAngle() / M_PI * 180.);
            break;
        case model::ActionType::SETUP_VEHICLE_PRODUCTION: LOG_INFO("Action: SETUP_VEHICLE_PRODUCTION");
            break;
        case model::ActionType::TACTICAL_NUCLEAR_STRIKE: LOG_INFO("Action: TACTICAL_NUCLEAR_STRIKE (%;%) by %", move.getX(),
                                                                  move.getY(),
                                                                  move.getVehicleId());
            break;
    }
#endif
}

void MyStrategy::calculating(const model::Player &me, const model::World &world, const model::Game &game,
                             model::Move &move) {
    std::list<std::shared_ptr<Action>> copy;
    while (!actions.empty()) {
        LOG_DEBUG("actions size %", actions.size());
        ActionResult actionResult = actions.front()->doSmth(*this);
        if (actionResult.result == WAIT) {
            break;
        }
        if (actionResult.result == DONE ||
            actionResult.result == ACTION_SET) {
            while (actionResult.count != 0) {
                actions.pop_front();
                --actionResult.count;
            }
        }
        if (actionResult.result == ACTION_SET) {
            break;
        }

        if (actionResult.result == SKIP) {
            while (actionResult.count != 0) {
                copy.push_back(actions.front());
                actions.pop_front();
                --actionResult.count;
            }
        }
    }

    while (!copy.empty()) {
        actions.push_front(copy.back());
        copy.pop_back();
    }
}


MyStrategy::MyStrategy() {

//    addActionBack(std::make_shared<I_JetInfluenceMapCalc>());
//    addActionBack(std::make_shared<I_HeliInfluenceMapCalc>());
//    addActionBack(std::make_shared<I_TankInfluenceMapCalc>());
//    addActionBack(std::make_shared<I_IFVInfluenceMapCalc>());
//    addActionBack(std::make_shared<I_AARVInfluenceMapCalc>());

    struct CalculateStartCoords : Action {

        CalculateStartCoords() : Action(false) {}

        ActionResult doAction(MyStrategy &myStrategy) override {
            vector<double> coords;
            for (auto vType : CalcUtil::vehicleTypes) {
                Point center = CalcUtil::getCenterCoords(VarStorage::myVehiclesByType[int(vType)]);
                coords.push_back(center.x);
                coords.push_back(center.y);
            }
            std::sort(coords.begin(), coords.end());
            coords.erase(std::unique(coords.begin(), coords.end()), coords.end());
            for (int i = 0; i != 3; ++i) {
                CalcUtil::startCoords[i] = coords[i];
            }
            CalcUtil::squadsDistance = coords[1] - coords[0];
            CalcUtil::startCoords[3] = coords[2] + CalcUtil::squadsDistance;
            return DONE;
        }
    };

    addActionBack(std::make_shared<CalculateStartCoords>());

    struct CheckStartingPositions : Action {

        CheckStartingPositions() : Action(false) {
        }

        ActionResult doAction(MyStrategy &myStrategy) override {
            for (int i = 0; i != 3; ++i) {
                for (int j = 0; j != 3; ++j) {
                    CalcUtil::startingPosition[i][j] = VehicleType::_UNKNOWN_;
                }
            }
            for (auto vType : CalcUtil::vehicleTypes) {
                Point center = CalcUtil::getCenterCoords(VarStorage::myVehiclesByType[int(vType)]);
                LOG_ERROR("% %", center.x, center.y);
                int ptX = (int) (center.x + .5);
                int ptY = (int) (center.y + .5);
                int i = 0;
                int j = 0;
                for (; i != 2; ++i) {
                    if (ptX == CalcUtil::startCoords[i]) {
                        break;
                    }
                }
                for (; j != 2; ++j) {
                    if (ptY == CalcUtil::startCoords[j]) {
                        break;
                    }
                }
                CalcUtil::startingPosition[i][j] = vType;
                myStrategy.blocks.emplace_back(int(vType) * 5 + 1,
                                               CalcUtil::isGround(vType),
                                               center,
                                               CalcUtil::getMaxDistance(VarStorage::myVehiclesByType[int(vType)],
                                                                        center) +
                                               CalcUtil::game.getVehicleRadius(),
                                               CalcUtil::getMaxSpeed(vType) * .6,
                                               CalcUtil::getMaxSpeed(vType));
            }
            return DONE;
        }
    };

    addActionBack(std::make_shared<CheckStartingPositions>());


////    struct WaitAmountOrCd : CdAction {
////        int amount;
////        int startedTick;
////
////        WaitAmountOrCd(int amount) : amount(amount) {}
////
////        int doAction(MyStrategy &myStrategy) {
////            if (startedTick + amount > VarStorage::currentWorld->getTickIndex()) {
////                return 0;
////            }
////            return -1;
////        }
////    };
////    struct WaitAmountOrCdIniter : Action {
////        WaitAmountOrCd *initTo;
////
////        WaitAmountOrCdIniter(WaitAmountOrCd *initTo, MyStrategy &myStrategy) : initTo(initTo) {
////            myStrategy.addActionBack(this);
////            myStrategy.addActionBack(initTo);
////        }
////
////        int doSmth(MyStrategy &myStrategy) {
////            initTo->startedTick = VarStorage::currentWorld->getTickIndex();
////            return -1;
////        }
////    };
//
////    new WaitAmountOrCdIniter(new WaitAmountOrCd(43), *this);
//
    struct SetGroup : Action {
        int groupNo;

        SetGroup(int groupNo) : Action(), groupNo(groupNo) {}

        ActionResult doAction(MyStrategy &myStrategy) override {
            VarStorage::currentMove->setAction(ActionType::ASSIGN);
            VarStorage::currentMove->setGroup(groupNo);
            return ACTION_SET;
        }
    };
//
    struct SelectNextCell : Action {

        VehicleType vehicleType;

        SelectNextCell(VehicleType vehicleType) : Action(), vehicleType(vehicleType) {
            auto action = std::make_shared<SetGroup>((int) (vehicleType) * 2);
            this->sendSignalTo(action);
        }

        ActionResult doAction(MyStrategy &myStrategy) override {
            std::vector<double> yCoords;
            for (auto &vehicle : VarStorage::myVehiclesByType[int(vehicleType)]) {
                yCoords.push_back(vehicle->position.y);
            }
            std::sort(yCoords.begin(), yCoords.end());

            VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
            VarStorage::currentMove->setVehicleType(vehicleType);
            VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
            VarStorage::currentMove->setTop(yCoords[yCoords.size() / 2] - 1e-6);
            VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
            return ACTION_SET;
        }
    };

    struct SelectNextCell2 : Action {

        VehicleType vehicleType;

        SelectNextCell2(VehicleType vehicleType) : Action(), vehicleType(vehicleType) {
            auto action = std::make_shared<SetGroup>((int) (vehicleType) * 2 + 1);
            this->sendSignalTo(action);
        }

        ActionResult doAction(MyStrategy &myStrategy) override {
            auto selected = CalcUtil::getSelectedUnits();
            double minY = 1e100;
            for (auto &vehicle : selected) {
                if (minY > vehicle->position.y) {
                    minY = vehicle->position.y;
                }
            }

            VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
            VarStorage::currentMove->setVehicleType(vehicleType);
            VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
            VarStorage::currentMove->setBottom(minY - 1e-6);
            return ACTION_SET;
        }
    };


    struct RotateCell : Action {

        ActionResult doAction(MyStrategy &myStrategy) override {
            auto selection = CalcUtil::getSelectedUnits();
            double maxX(0), sumY(0);
            for (auto &vehicle : selection) {
                sumY += vehicle->position.y;
                if (vehicle->position.x > maxX) {
                    maxX = vehicle->position.x;
                }
            }
            Point pt = Point(maxX + 2, sumY / selection.size());
            LOG_DEBUG("% % %", selection.size(), pt.x, pt.y);
            VarStorage::currentMove->setAction(ActionType::ROTATE);
            VarStorage::currentMove->setAngle(-Constants::PI_4);
            VarStorage::currentMove->setMaxAngularSpeed(CalcUtil::getMaxRotationSpeed(selection, pt));
            VarStorage::currentMove->setX(pt.x);
            VarStorage::currentMove->setY(pt.y);

            return ACTION_SET;
        }
    };

    struct SelectAllVehicleByType : Action {
        VehicleType vehicleType;

        SelectAllVehicleByType(VehicleType vehicleType) : Action(), vehicleType(vehicleType) {
        }

        ActionResult doAction(MyStrategy &myStrategy) override {
            bool ok = true;
            for (auto &veh : VarStorage::myVehiclesVc) {
                if ((veh->vType == vehicleType) ^ (veh->selected)) {
                    ok = false;
                    break;
                }
            }
            if (ok) {
                LOG_WARN("already selected");
                return DONE;
            }
            VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
            VarStorage::currentMove->setVehicleType(vehicleType);
            VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
            VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
            return ACTION_SET;
        }
    };
//
    struct Scale : Action {

        ActionResult doAction(MyStrategy &myStrategy) override {
            auto selected = CalcUtil::getSelectedUnits();

            Point pt = CalcUtil::getCenterCoords(selected);
            VarStorage::currentMove->setAction(ActionType::SCALE);
            VarStorage::currentMove->setFactor(2. / 3. + 1e-3);
            VarStorage::currentMove->setMaxSpeed(CalcUtil::getMaxSpeed(selected[0]->vType) * .6);
            VarStorage::currentMove->setX(pt.x);
            VarStorage::currentMove->setY(pt.y);
            return ACTION_SET;
        }
    };

    {
        for (int i = 0; i != 5; ++i) {
            auto selectAll = std::make_shared<SelectAllVehicleByType>(CalcUtil::vehicleTypes[i]);

            auto scale = std::make_shared<Scale>();
            selectAll->sendSignalTo(scale);

            scale->sendSignalTo(std::make_shared<SetGroup>(int(CalcUtil::vehicleTypes[i]) * 5 + 1));

//            auto selectNextCell = std::make_shared<SelectNextCell>(CalcUtil::vehicleTypes[i]);
//            scale->sendSignalTo(selectNextCell);
//
//            auto selectNextCell2 = std::make_shared<SelectNextCell2>(CalcUtil::vehicleTypes[i]);
//            selectNextCell->sendSignalTo(selectNextCell2);

            addActionBack(selectAll);
        }
    }

    {
        auto selectAll = std::make_shared<SelectAllVehicleByType>(VehicleType::TANK);
        addActionBack(selectAll);
    }

    struct JetControl : Action {

        ActionResult doAction(MyStrategy &myStrategy) override {
            auto start = std::chrono::steady_clock::now();

            const int myGroup = int(model::VehicleType::FIGHTER) * 5 + 1;
            if (myStrategy.groups[myGroup].empty()) {
                return DONE;
            }
            bool hasMoved = false;
            bool hasOne = false;
            for (auto &vehicle : myStrategy.groups[myGroup]) {
                if (vehicle->moved) {
                    hasMoved = true;
                }
                if (vehicle->expectedTicksForMovement > 1 && vehicle->moved) {
                    return SKIP;
                } else if (vehicle->expectedTicksForMovement == 1) {
                    hasOne = true;
                }
            }
            bool selected = CalcUtil::isGroupSelected(myGroup);
            if (hasMoved && (hasOne && selected)) {
                return WAIT;
            }

            if (!selected) {
                auto &move = VarStorage::currentMove;
                move->setAction(ActionType::CLEAR_AND_SELECT);
                move->setGroup(myGroup);
                return WAIT;
            }
            I_JetInfluenceMapCalc::calculateJetField(myStrategy);

            Point center = CalcUtil::getCenterCoords(myStrategy.groups[myGroup]);
            double distance = CalcUtil::getMaxDistance(myStrategy.groups[myGroup], center);

            GroupBlocks *strategyBlock = nullptr;
            GroupBlocks searchBlock;
            for (auto &block : myStrategy.blocks) {
                if (block.groupId == myGroup) {
                    searchBlock = block;
                    strategyBlock = &block;
                    searchBlock.currentPosMax = center;
                    searchBlock.currentPosMin = center;
                    searchBlock.dest = center;
                    searchBlock.radius = distance + CalcUtil::game.getVehicleRadius();
                    break;
                }
            }
            if (strategyBlock == nullptr) {
                LOG_ERROR("currentBlock not found!");
                return SKIP;
            }

            // find direction
            const double xPerCell = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
            const double yPerCell = CalcUtil::game.getWorldHeight() / Constants::influenceMapSize;

            const int directionsCount = Constants::directionsCount;
            const int ticksPerSim = 1;

//            auto maxTicksCount =
//                    int((1. / VarStorage::jetInfluenceField[4][int(center.x / xPerCell)][int(center.y / yPerCell)]) * 48. * myStrategy.groups[myGroup].size() /
//                        100);
            auto maxTicksCount = 200;
            // 0.25959752283430748 - stop ? // 0.17263676668745123 - 70 ticks?
            if (maxTicksCount < 30) {
                maxTicksCount = 30;
            }
            LOG_DEBUG("max tick count %", maxTicksCount);
            const int minTicksCount = maxTicksCount / 2;

            int bestAngle = -1;
            double bestGlobalScore = -1e100;
            int ticksToMove = 0;
            for (int currentDirection = 0; currentDirection != directionsCount; ++currentDirection) {
                Point maxSimPoint = center;
                Point minSimPoint = center;
                double angle = CalcUtil::getAngle(currentDirection);
                Point vector = {std::cos(angle), std::sin(angle)};
                VarStorage::groupBlocks.clear();
                { // copy blocks for simulation
                    for (auto &block : myStrategy.blocks) {
                        if (block.groupId != myGroup && block.ground == searchBlock.ground) {
                            VarStorage::groupBlocks.push_back(block);
                        }
                    }
                }
                int tickCount = 0;
                bool colisionFound = false;

                double minValue = std::numeric_limits<double>::infinity();
                double maxValue = -minValue;

                double bestScore = -std::numeric_limits<double>::infinity();
                int bestTick = 0;

                while (tickCount < maxTicksCount) {
                    minSimPoint += vector * searchBlock.minSpeed * ticksPerSim;
                    maxSimPoint += vector * searchBlock.maxSpeed * ticksPerSim;
                    for (auto &block : VarStorage::groupBlocks) { // move all blocks
                        double minDistance = block.radius + searchBlock.radius;
                        if (!block.currentPosMin.equalsValues(block.dest)) { // still moving
                            Point diff = block.dest - block.currentPosMin;
                            Point move = block.onePointVector * block.minSpeed * ticksPerSim;
                            if (abs(diff.x) >= abs(move.x) && abs(diff.y) >= abs(move.y)) {
                                block.currentPosMin = block.dest;
                            } else {
                                block.currentPosMin += move;
                                if (!block.currentPosMax.equalsValues(block.dest)) { // still moving
                                    move = block.onePointVector * block.maxSpeed * ticksPerSim;
                                    if (abs(diff.x) >= abs(move.x) && abs(diff.y) >= abs(move.y)) {
                                        block.currentPosMax = block.dest;
                                    } else {
                                        block.currentPosMax += move;
                                    }
                                }
                            }
                            if (CalcUtil::distancePointToSegment(block.dest, minSimPoint, maxSimPoint) <= minDistance) {
                                colisionFound = true;
                                break;
                            }
                        } else {
                            if (CalcUtil::distancePointToSegment(minSimPoint, block.currentPosMin,
                                                                 block.currentPosMax) <= minDistance ||
                                CalcUtil::distancePointToSegment(maxSimPoint, block.currentPosMin,
                                                                 block.currentPosMax) <= minDistance) {
                                colisionFound = true;
                                break;
                            }
                        }

                    }
                    if (colisionFound) {
                        break;
                    }
                    if (maxSimPoint.x < searchBlock.radius || maxSimPoint.y < searchBlock.radius ||
                        maxSimPoint.x + searchBlock.radius >= CalcUtil::game.getWorldWidth() ||
                        maxSimPoint.y + searchBlock.radius >= CalcUtil::game.getWorldHeight()) {
                        if (isinf(bestScore)) {
                            bestScore = 1e-100;
                            bestTick = tickCount;
                        }
                        break;
                    }

                    const auto fromX = int(maxSimPoint.x / xPerCell);
                    const auto fromY = int(maxSimPoint.y / yPerCell);
                    double val = VarStorage::jetInfluenceField[0][fromX][fromY];
                    if (minValue > val) {
                        minValue = val;
                    }
                    if (maxValue < val) {
                        maxValue = val;
                    }

                    tickCount += ticksPerSim;
                    double score = minValue + maxValue;
                    if (score >= bestScore && tickCount >= minTicksCount) {
                        bestTick = tickCount;
                        bestScore = score;
                    }
                }

                if (!colisionFound) {
                    if (bestGlobalScore < bestScore) {
                        bestGlobalScore = bestScore;
                        bestAngle = currentDirection;
                        ticksToMove = bestTick;
                    }
                }
            }

            if (bestAngle == -1) {
                LOG_ERROR("path not found!");
                return SKIP;

            }
            auto now = std::chrono::steady_clock::now();
            long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
            LOG_DEBUG("% microseconds", times);

//            auto val = CalcUtil::checkWaiting(myStrategy.groups[myGroup], *strategyBlock);
            double angle = CalcUtil::getAngle(bestAngle);
//            if (val.expectedMyDeaths * 3 <= val.expectedEnemyDeaths) {
//                //keep going
//                val = CalcUtil::checkDirection(bestAngle, myStrategy.groups[myGroup], searchBlock, ticksToMove);
//                ticksToMove = val.ticksCount;
//            }
            strategyBlock->currentPosMin = center;
            strategyBlock->currentPosMax = center;
            strategyBlock->radius = distance + CalcUtil::game.getVehicleRadius();
            strategyBlock->onePointVector = {std::cos(angle), std::sin(angle)};
            Point vector = strategyBlock->onePointVector * (ticksToMove * strategyBlock->maxSpeed);
            strategyBlock->dest = center + vector;
            auto &move = VarStorage::currentMove;
            move->setAction(ActionType::MOVE);
            move->setX(vector.x);
            move->setY(vector.y);

            now = std::chrono::steady_clock::now();
            times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
            LOG_DEBUG("% microseconds after check", times);
            return WAIT;
        }
    };

    struct HeliControl : Action {

        ActionResult doAction(MyStrategy &myStrategy) override {
            auto start = std::chrono::steady_clock::now();

            const int myGroup = int(model::VehicleType::HELICOPTER) * 5 + 1;
            if (myStrategy.groups[myGroup].empty()) {
                return DONE;
            }
            bool hasMoved = false;
            bool hasOne = false;
            for (auto &vehicle : myStrategy.groups[myGroup]) {
                if (vehicle->moved) {
                    hasMoved = true;
                }
                if (vehicle->expectedTicksForMovement > 1 && vehicle->moved) {
                    return SKIP;
                } else if (vehicle->expectedTicksForMovement == 1) {
                    hasOne = true;
                }
            }
            bool selected = CalcUtil::isGroupSelected(myGroup);
            if (hasMoved && (hasOne && selected)) {
                return WAIT;
            }

            if (!selected) {
                auto &move = VarStorage::currentMove;
                move->setAction(ActionType::CLEAR_AND_SELECT);
                move->setGroup(myGroup);
                return WAIT;
            }
            I_HeliInfluenceMapCalc::calculateHeliField(myStrategy);

            Point center = CalcUtil::getCenterCoords(myStrategy.groups[myGroup]);
            double distance = CalcUtil::getMaxDistance(myStrategy.groups[myGroup], center);

            GroupBlocks *strategyBlock = nullptr;
            GroupBlocks searchBlock;
            for (auto &block : myStrategy.blocks) {
                if (block.groupId == myGroup) {
                    searchBlock = block;
                    strategyBlock = &block;
                    searchBlock.currentPosMax = center;
                    searchBlock.currentPosMin = center;
                    searchBlock.dest = center;
                    searchBlock.radius = distance + CalcUtil::game.getVehicleRadius();
                    break;
                }
            }
            if (strategyBlock == nullptr) {
                LOG_ERROR("currentBlock not found!");
                return SKIP;
            }

            // find direction
            const double xPerCell = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
            const double yPerCell = CalcUtil::game.getWorldHeight() / Constants::influenceMapSize;

            const int directionsCount = Constants::directionsCount;
            const int ticksPerSim = 1;

//            auto maxTicksCount =
//                    int((1. / VarStorage::heliInfluenceField[4][int(center.x / xPerCell)][int(center.y / yPerCell)]) * 48. * myStrategy.groups[myGroup].size() /
//                        100);
//            // 0.25959752283430748 - stop ? // 0.17263676668745123 - 70 ticks?
//            if (maxTicksCount < 30) {
//                maxTicksCount = 30;
//            }
            auto maxTicksCount = 200;
            LOG_DEBUG("max tick count %", maxTicksCount);
            const int minTicksCount = maxTicksCount / 2;

            int bestAngle = -1;
            double bestGlobalScore = -std::numeric_limits<double>::infinity();
            int ticksToMove = 0;
            for (int currentDirection = 0; currentDirection != directionsCount; ++currentDirection) {
                Point maxSimPoint = center;
                Point minSimPoint = center;
                double angle = CalcUtil::getAngle(currentDirection);
                Point vector = {std::cos(angle), std::sin(angle)};
                VarStorage::groupBlocks.clear();
                { // copy blocks for simulation
                    for (auto &block : myStrategy.blocks) {
                        if (block.groupId != myGroup && block.ground == searchBlock.ground) {
                            VarStorage::groupBlocks.push_back(block);
                        }
                    }
                }
                int tickCount = 0;
                bool colisionFound = false;

                double minValue = std::numeric_limits<double>::infinity();
                double maxValue = -minValue;

                double bestScore = -std::numeric_limits<double>::infinity();
                int bestTick = 0;

                while (tickCount < maxTicksCount) {
                    minSimPoint += vector * searchBlock.minSpeed * ticksPerSim;
                    maxSimPoint += vector * searchBlock.maxSpeed * ticksPerSim;
                    for (auto &block : VarStorage::groupBlocks) { // move all blocks
                        double minDistance = block.radius + searchBlock.radius;
                        minDistance *= minDistance;
                        if (!block.currentPosMin.equalsValues(block.dest)) { // still moving
                            Point diff = block.dest - block.currentPosMin;
                            Point move = block.onePointVector * block.minSpeed * ticksPerSim;
                            if (abs(diff.x) >= abs(move.x) && abs(diff.y) >= abs(move.y)) {
                                block.currentPosMin = block.dest;
                            } else {
                                block.currentPosMin += move;
                                if (!block.currentPosMax.equalsValues(block.dest)) { // still moving
                                    move = block.onePointVector * block.maxSpeed * ticksPerSim;
                                    if (abs(diff.x) >= abs(move.x) && abs(diff.y) >= abs(move.y)) {
                                        block.currentPosMax = block.dest;
                                    } else {
                                        block.currentPosMax += move;
                                    }
                                }
                            }
                            if (CalcUtil::sqrDistancePointToSegment(block.dest, minSimPoint, maxSimPoint) <= minDistance) {
                                colisionFound = true;
                                break;
                            }
                        } else {
                            if (CalcUtil::sqrDistancePointToSegment(minSimPoint, block.currentPosMin,
                                                                    block.currentPosMax) <= minDistance ||
                                CalcUtil::sqrDistancePointToSegment(maxSimPoint, block.currentPosMin,
                                                                    block.currentPosMax) <= minDistance) {
                                colisionFound = true;
                                break;
                            }
                        }

                    }
                    if (colisionFound) {
                        break;
                    }
                    if (maxSimPoint.x < searchBlock.radius || maxSimPoint.y < searchBlock.radius ||
                        maxSimPoint.x + searchBlock.radius >= CalcUtil::game.getWorldWidth() ||
                        maxSimPoint.y + searchBlock.radius >= CalcUtil::game.getWorldHeight()) {
                        if (isinf(bestScore)) {
                            bestScore = -1e100;
                            bestTick = tickCount;
                        }
                        break;
                    }

                    const auto fromX = int(maxSimPoint.x / xPerCell);
                    const auto fromY = int(maxSimPoint.y / yPerCell);
                    double val = VarStorage::heliInfluenceField[0][fromX][fromY];
                    if (minValue > val) {
                        minValue = val;
                    }
                    if (maxValue < val) {
                        maxValue = val;
                    }

                    tickCount += ticksPerSim;
                    double score = minValue + maxValue;
                    if (score >= bestScore && tickCount >= minTicksCount) {
                        bestTick = tickCount;
                        bestScore = score;
                    }
                }

                if (!colisionFound) {
                    if (bestGlobalScore < bestScore) {
                        bestGlobalScore = bestScore;
                        bestAngle = currentDirection;
                        ticksToMove = bestTick;
                    }
                }
            }

            if (bestAngle == -1) {
                LOG_ERROR("path not found!");
                return SKIP;

            }
            auto now = std::chrono::steady_clock::now();
            long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
            LOG_DEBUG("% microseconds", times);

//            auto val = CalcUtil::checkWaiting(myStrategy.groups[myGroup], *strategyBlock);
            double angle = CalcUtil::getAngle(bestAngle);
//            if (val.expectedMyDeaths * 3 <= val.expectedEnemyDeaths) {
//                //keep going
//                val = CalcUtil::checkDirection(bestAngle, myStrategy.groups[myGroup], searchBlock, ticksToMove);
//                ticksToMove = val.ticksCount;
//            }
            strategyBlock->currentPosMin = center;
            strategyBlock->currentPosMax = center;
            strategyBlock->radius = distance + CalcUtil::game.getVehicleRadius();
            strategyBlock->onePointVector = {std::cos(angle), std::sin(angle)};
            Point vector = strategyBlock->onePointVector * (ticksToMove * strategyBlock->maxSpeed);
            strategyBlock->dest = center + vector;
            auto &move = VarStorage::currentMove;
            move->setAction(ActionType::MOVE);
            move->setX(vector.x);
            move->setY(vector.y);

            now = std::chrono::steady_clock::now();
            times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
            LOG_DEBUG("% microseconds after check", times);
            return WAIT;
        }
    };

    addActionBack(std::make_shared<JetControl>());
    addActionBack(std::make_shared<HeliControl>());

//    for (int i = 0; i != 5; ++i) {
//        auto select = std::make_shared<SelectAllVehicleByType>(CalcUtil::vehicleTypes[i]);
//        (*actions.rbegin())->sendSignalTo(select);
//
//        auto rotateCell = std::make_shared<RotateCell>();
//        select->sendSignalTo(rotateCell);
//    }
//
//    struct PiatnashkiMove : Action {
//
//        int dX, dY;
//
//        PiatnashkiMove(int dX, int dY) : dX(dX), dY(dY) {}
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            VarStorage::currentMove->setAction(ActionType::MOVE);
//            VarStorage::currentMove->setX(dX * CalcUtil::squadsDistance);
//            VarStorage::currentMove->setY(dY * CalcUtil::squadsDistance);
//            return ACTION_SET;
//        }
//    };
//
//    struct PiatnashkiFullSingleMove : Action {
//        std::vector<model::VehicleType> vehicleType;
//        std::vector<std::pair<int, int>> direction;
//        std::shared_ptr<Action> nextMove;
//
//        PiatnashkiFullSingleMove(const vector<VehicleType> &vehicleType, const vector<pair<int, int>> &direction)
//                : vehicleType(vehicleType), direction(direction) {
//        }
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            for (auto &item : vehicleType) { // wait till all vehicles ready for nest step
//                if (!VarStorage::myMovedVehiclesIdsByType[int(item)].empty()) {
//                    return SKIP;//skip for more useful actions
//                }
//            }
//            LOG_DEBUG("execute full single move");
//            auto iter = myStrategy.actions.begin();
//            ++iter;
//            for (size_t i = 0; i != vehicleType.size(); ++i) {
//                if (direction[i].first == 0 && direction[i].second == 0) {
//                    LOG_DEBUG("skip moving vType %", int(vehicleType[i]));
//                    continue; //this vType not moving this step
//                }
//                LOG_DEBUG("create PiatnashkiSelectAndMove % % %", int(vehicleType[i]), direction[i].first, direction[i].second);
//                SelectAllVehicleByType *selectAllVehicleByType = new SelectAllVehicleByType(vehicleType[i]);
//                auto shared = std::make_shared<Action>(selectAllVehicleByType);
//                shared->sendSignalTo(std::make_shared<Action>(new PiatnashkiMove(direction[i].first, direction[i].second)));
//                myStrategy.addActionAfter(iter, shared);
//            }
//            if (nextMove.get() != nullptr) {
//                myStrategy.addActionAfter(iter, nextMove);
//            }
//            return DONE;
//        }
//    };
//
//    struct PiatnashkiCalc : Action {
//
//        std::vector<model::VehicleType> vehicleTypes;
//        std::vector<std::pair<int, int>> position;
//        std::shared_ptr<Action> moveAfterAction;
//
//        PiatnashkiCalc(const vector<VehicleType> &vehicleTypes, const vector<pair<int, int>> &position)
//                : vehicleTypes(vehicleTypes), position(position) {}
//
//        PiatnashkiCalc(const vector<VehicleType> &vehicleTypes, const vector<pair<int, int>> &position, std::shared_ptr<Action> moveAfterAction)
//                : vehicleTypes(vehicleTypes), position(position), moveAfterAction(moveAfterAction) {}
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            std::vector<std::pair<int, int>> from;
//            from.reserve(vehicleTypes.size());
//            for (auto &vType : vehicleTypes) {
//                int i = 0;
//                int j = 0;
//                CalcUtil::getStartingCoords(vType, i, j);
//                from.emplace_back(i, j);
//            }
//            for (size_t j = 0; j != vehicleTypes.size(); ++j) {
//                LOG_DEBUG("(% %)", from[j].first, from[j].second);
//            }
//            LOG_DEBUG("===FROM^===")
//            for (size_t j = 0; j != vehicleTypes.size(); ++j) {
//                LOG_DEBUG("(% %)", position[j].first, position[j].second);
//            }
//            LOG_DEBUG("=====TO^=====")
//
//            auto rez = Piatnashki::getPiatnashkiMovements(from, position);
//            std::shared_ptr<PiatnashkiFullSingleMove> first;
//            std::shared_ptr<PiatnashkiFullSingleMove> last;
//            for (size_t i = 0; i != rez.size(); ++i) {
//                for (size_t j = 0; j != vehicleTypes.size(); ++j) {
//                    LOG_DEBUG("(% %)", rez[i][j].first, rez[i][j].second);
//                }
//                LOG_DEBUG("============")
//            }
//            for (size_t i = 1; i != rez.size(); ++i) {
//                std::vector<std::pair<int, int>> direction;
//
//                for (size_t j = 0; j != vehicleTypes.size(); ++j) {
//                    direction.emplace_back(rez[i][j].first - rez[i - 1][j].first, rez[i][j].second - rez[i - 1][j].second);
//                }
//
//                auto current = std::make_shared<PiatnashkiFullSingleMove>(new PiatnashkiFullSingleMove(vehicleTypes, direction));
//                if (!first) {
//                    first = current;
//                }
//                if (last) {
//                    last->nextMove = current;
//                }
//                last = current;
//            }
//            if (moveAfterAction) {
//                if (last) {
//                    last->nextMove = moveAfterAction;
//                } else {
//                    auto iter = myStrategy.actions.begin();
//                    ++iter;
//                    myStrategy.addActionAfter(iter, moveAfterAction);
//                }
//            }
//            if (first != nullptr) {
//                auto iter = myStrategy.actions.begin();
//                ++iter;
//                myStrategy.addActionAfter(iter, first);
//            }
//
//            return DONE;
//        }
//    };
////
//    struct HalfPiatnashkiMove : Action {
//
//        int dX, dY;
//
//        HalfPiatnashkiMove(int dX, int dY) : Action(), dX(dX), dY(dY) {}
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            VarStorage::currentMove->setAction(ActionType::MOVE);
//            VarStorage::currentMove->setX(dX * CalcUtil::squadsDistance * .5);
//            VarStorage::currentMove->setY(dY * CalcUtil::squadsDistance * .5);
//            return ACTION_SET;
//        }
//    };
//
//    struct AfterPiatnashkiSelectAndMove : Action {
//        int dX, dY, cX, cY;
//
//        AfterPiatnashkiSelectAndMove(int dX, int dY, int cX, int cY) : Action(), dX(dX), dY(dY), cX(cX), cY(cY) {
//            auto halfPiatnashkiMove = std::make_shared<Action>(new HalfPiatnashkiMove(dX, dY));
//            this->sendSignalTo(halfPiatnashkiMove);
//        }
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
//            VarStorage::currentMove->setLeft(CalcUtil::startCoords[cX] - CalcUtil::squadsDistance * .5);
//            VarStorage::currentMove->setTop(CalcUtil::startCoords[cY] - CalcUtil::squadsDistance * .5);
//            VarStorage::currentMove->setRight(CalcUtil::startCoords[cX] + CalcUtil::squadsDistance * .5);
//            VarStorage::currentMove->setBottom(CalcUtil::startCoords[cY] + CalcUtil::squadsDistance * .6);
//            return ACTION_SET;
//        }
//    };
//
//    struct AfterPiatnashkiRightCornerPrePack : Action {
//        AfterPiatnashkiRightCornerPrePack() {
//            auto sm = std::make_shared<Action>(new AfterPiatnashkiSelectAndMove(-1, -1, 2, 2));
//            this->sendSignalTo(sm);
//        }
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            const VehicleType vTypes[] = {VehicleType::FIGHTER, VehicleType::ARRV, VehicleType::IFV, VehicleType::HELICOPTER};
//            for (auto &item : vTypes) { // wait till all vehicles ready for nest step
//                if (!VarStorage::myMovedVehiclesIdsByType[int(item)].empty()) {
//                    return SKIP;//skip for more useful actions
//                }
//            }
//            return DONE;
//        }
//    };
//
//    struct AfterPiatnashkiLeftCornerPrePack : Action {
//
//        AfterPiatnashkiLeftCornerPrePack() {
//            auto sm = std::make_shared<Action>(new AfterPiatnashkiSelectAndMove(1, 1, 0, 0));
//            this->sendSignalTo(sm);
//        }
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            const VehicleType vTypes[] = {VehicleType::TANK, VehicleType::IFV, VehicleType::HELICOPTER};
//            for (auto &item : vTypes) { // wait till all vehicles ready for nest step
//                if (!VarStorage::myMovedVehiclesIdsByType[int(item)].empty()) {
//                    return SKIP;//skip for more useful actions
//                }
//            }
//            return DONE;
//        }
//    };
//
//    struct EmptyAction : Action {
//
//        EmptyAction(std::vector<std::shared_ptr<Action>> signalToActions) : Action(false) {
//            for (auto &action : signalToActions) {
//                this->sendSignalTo(action);
//            }
//        }
//
//        ActionResult doAction(MyStrategy &myStrategy) override {
//            return DONE;
//        }
//    };
//
//    auto prepare = std::make_shared<Action>(new AfterPiatnashkiRightCornerPrePack());
//    auto prepare2 = std::make_shared<Action>(new AfterPiatnashkiLeftCornerPrePack());
//
//    addActionBack(new PiatnashkiCalc({model::VehicleType::TANK, model::VehicleType::ARRV, model::VehicleType::IFV},
//                                     {pair<int, int>(0, 0), pair<int, int>(2, 2), pair<int, int>(1, 1)},
//                                     std::make_shared<Action>(new EmptyAction({prepare, prepare2}))));
//    addActionBack(new PiatnashkiCalc({model::VehicleType::HELICOPTER, model::VehicleType::FIGHTER},
//                                     {pair<int, int>(1, 1), pair<int, int>(2, 2)},
//                                     std::make_shared<Action>(new EmptyAction({prepare, prepare2}))));
////
////    struct SwapTankWithARRV : Action {
////
////        ActionResult doAction(MyStrategy &myStrategy) override {
////            bool testPassed = false;
////            if (VarStorage::currentWorld->getTickIndex() > 1800) {
////                testPassed = true;
////            } else {
////                auto vc = CalcUtil::getCounts(myStrategy.enemyVehicles);
////                if (vc[int(VehicleType::FIGHTER)] < 5 && vc[int(VehicleType::HELICOPTER)] < 5) {
////                    testPassed = true;
////                }
////            }
//////            if (!testPassed) {//test passed if all of them too far
//////                testPassed = true;
//////                for (auto &vehicle : myStrategy.enemyVehicles) {
//////                    double dX = vehicle.second.x - 200;
//////                    double dY = vehicle.second.y - 200;
//////                    if (dX * dX + dY * dY < 500 * 500) {
//////                        testPassed = false;
//////                        break;
//////                    }
//////                }
//////            }
////            if (testPassed) {
////                LOG_DEBUG("start rotating around!");
//////                auto iterator = myStrategy.actions.begin();
//////                ++iterator;
//////                myStrategy.addActionAfter(iterator, new OrderedAction({[](MyStrategy &myStrategy) {
//////                    VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
//////                    VarStorage::currentMove->setVehicleType(VehicleType::TANK);
//////                    VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
//////                    VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
//////                }, [](MyStrategy &myStrategy) {
//////                    VarStorage::currentMove->setAction(ActionType::ADD_TO_SELECTION);
//////                    VarStorage::currentMove->setVehicleType(VehicleType::ARRV);
//////                    VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
//////                    VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
//////                }, [](MyStrategy &myStrategy) {
//////                    vector<MyVehicle *> selected;
//////                    CalcUtil::getSelectedUnits(myStrategy.myVehicles, selected);
//////                    VarStorage::currentMove->setAction(ActionType::ROTATE);
//////                    int both = 0;
//////                    Point pt = Point(124.86, 133.14);
//////                    for (auto &vehicle : myStrategy.myVehicles) {
//////                        if (vehicle.second.vType != VehicleType::HELICOPTER) {
//////                            continue;
//////                        }
//////                        if (!(both & 1) && abs(vehicle.second.x - pt.getX()) < 2) {
//////                            both |= 1;
//////                            pt.setX(vehicle.second.x);
//////                        }
//////                        if (!(both & 2) && abs(vehicle.second.y - pt.getY()) < 2) {
//////                            both |= 2;
//////                            pt.setY(vehicle.second.y);
//////                        }
//////                        if (both == 3) {
//////                            break;
//////                        }
//////                    }
//////                    VarStorage::currentMove->setMaxAngularSpeed(CalcUtil::getMaxRotationSpeed(selected, pt));
//////                    VarStorage::currentMove->setAngle(M_PI);
//////                    VarStorage::currentMove->setX(pt.getX());
//////                    VarStorage::currentMove->setY(pt.getY());
//////                    LOG_DEBUG("rotate around % %", pt.getX(), pt.getY());
//////                }}));
////
////                return DONE;
////            }
////            return SKIP;
////        }
////    };
//////    struct NucklearHerak : public Action {
//////
//////        ActionResult doAction(MyStrategy &myStrategy) override {
//////            if (VarStorage::currentPlayer->getRemainingNuclearStrikeCooldownTicks()) {
//////                return SKIP;
//////            }
//////            Point pt = Point(161.86, 170.14);
//////            for (auto &unit : myStrategy.myVehicles) {
//////                if (unit.second.vType != VehicleType::ARRV) {
//////                    continue;
//////                }
//////                double d1 = unit.second.x - pt.getX();
//////                double d2 = unit.second.y - pt.getY();
//////                double distance = d1 * d1 + d2 * d2;
//////                if (distance < 30) {
//////                    VarStorage::currentMove->setAction(ActionType::TACTICAL_NUCLEAR_STRIKE);
//////                    VarStorage::currentMove->setVehicleId(unit.first);
//////                    VarStorage::currentMove->setX(pt.getX());
//////                    VarStorage::currentMove->setY(pt.getY());
//////                    LOG_DEBUG("CABOOOOM!");
//////                    return ACTION_SET;
//////                }
//////            }
//////            return SKIP;
//////        }
//////    };
//////
//////    NucklearHerak *herak = new NucklearHerak();
//////    herak->waitForSignalOf(prepare);
//////    NucklearHerak *herak2 = new NucklearHerak();
//////    herak2->waitForSignalOf(herak);
//////    NucklearHerak *herak3 = new NucklearHerak();
//////    herak3->waitForSignalOf(herak2);
////    auto swapTankWithARRV = std::make_shared<Action>(new SwapTankWithARRV());
////    prepare->sendSignalTo(swapTankWithARRV);
////    prepare2->sendSignalTo(swapTankWithARRV);
////
////    struct ActAfterStop : Action {
////        VehicleType vehicleType;
////
////        ActAfterStop(VehicleType vehicleType) : vehicleType(vehicleType) {}
////
////        ActionResult doAction(MyStrategy &myStrategy) override {
////            if (!VarStorage::myMovedVehiclesIdsByType[int(vehicleType)].empty()) {
////                return SKIP;//skip for more useful actions
////            }
////            return DONE;
////        }
////    };
////    {
////        VehicleType vTypes[] = {VehicleType::TANK, VehicleType::ARRV};
////        int coeff = 1;
////        for (int i = 0; i != 2; ++i) {
////            ActAfterStop *tanksActAfterStop = new ActAfterStop(vTypes[i]);
////            tanksActAfterStop->waitForSignalOf(swapTankWithARRV);
////            SelectAllVehicleByType *selection1 = new SelectAllVehicleByType(vTypes[i]);
////            selection1->waitForSignalOf(tanksActAfterStop);
////            HalfPiatnashkiMove *halfPiatnashkiMove1 = new HalfPiatnashkiMove(-1 * coeff, 1 * coeff);
////            halfPiatnashkiMove1->waitForSignalOf(selection1);
////
////            ActAfterStop *tanksActAfterStop2 = new ActAfterStop(vTypes[i]);
////            tanksActAfterStop2->waitForSignalOf(halfPiatnashkiMove1);
////            SelectAllVehicleByType *selection2 = new SelectAllVehicleByType(vTypes[i]);
////            selection2->waitForSignalOf(tanksActAfterStop2);
////            HalfPiatnashkiMove *halfPiatnashkiMove2 = new HalfPiatnashkiMove(2 * coeff, 2 * coeff);
////            halfPiatnashkiMove2->waitForSignalOf(selection2);
////
////            ActAfterStop *tanksActAfterStop3 = new ActAfterStop(vTypes[i]);
////            tanksActAfterStop3->waitForSignalOf(halfPiatnashkiMove2);
////            SelectAllVehicleByType *selection3 = new SelectAllVehicleByType(vTypes[i]);
////            selection3->waitForSignalOf(tanksActAfterStop3);
////            HalfPiatnashkiMove *halfPiatnashkiMove3 = new HalfPiatnashkiMove(1 * coeff, -1 * coeff);
////            halfPiatnashkiMove3->waitForSignalOf(selection3);
////            coeff *= -1;
////        }
////    }
////    {
////        ActAfterStop *tanksActAfterStop = new ActAfterStop(VehicleType::TANK);
////        tanksActAfterStop->waitForSignalOf(swapTankWithARRV);
////        ActAfterStop *arrvActAfterStop = new ActAfterStop(VehicleType::ARRV);
////        arrvActAfterStop->waitForSignalOf(tanksActAfterStop);
////
////
////    }
//
//    /*
//     * myStrategy.addActionAfter(iterator, new OrderedAction({[](MyStrategy &myStrategy) {
//                    VarStorage::currentMove->setAction(ActionType::CLEAR_AND_SELECT);
//                    VarStorage::currentMove->setVehicleType(VehicleType::TANK);
//                    VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
//                    VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
//                }, [](MyStrategy &myStrategy) {
//                    VarStorage::currentMove->setAction(ActionType::ADD_TO_SELECTION);
//                    VarStorage::currentMove->setVehicleType(VehicleType::ARRV);
//                    VarStorage::currentMove->setRight(CalcUtil::game.getWorldWidth());
//                    VarStorage::currentMove->setBottom(CalcUtil::game.getWorldHeight());
//                }, [](MyStrategy &myStrategy) {
//                    vector<MyVehicle *> selected;
//                    CalcUtil::getSelectedUnits(myStrategy.myVehicles, selected);
//                    VarStorage::currentMove->setAction(ActionType::ROTATE);
//                    int both = 0;
//                    Point pt = Point(124.86, 133.14);
//                    for (auto &vehicle : myStrategy.myVehicles) {
//                        if (vehicle.second.vType != VehicleType::HELICOPTER) {
//                            continue;
//                        }
//                        if (!(both & 1) && abs(vehicle.second.x - pt.getX()) < 2) {
//                            both |= 1;
//                            pt.setX(vehicle.second.x);
//                        }
//                        if (!(both & 2) && abs(vehicle.second.y - pt.getY()) < 2) {
//                            both |= 2;
//                            pt.setY(vehicle.second.y);
//                        }
//                        if (both == 3) {
//                            break;
//                        }
//                    }
//                    VarStorage::currentMove->setMaxAngularSpeed(CalcUtil::getMaxRotationSpeed(selected, pt));
//                    VarStorage::currentMove->setAngle(M_PI);
//                    VarStorage::currentMove->setX(pt.getX());
//                    VarStorage::currentMove->setY(pt.getY());
//     */
////
////    {
////        SelectAllVehicleByType *selectTanks = new SelectAllVehicleByType(VehicleType::TANK);
////
////    }
//

}

//http://russianaicup.ru/s/1510219135187/assets/jsrenderer/wasm32/assets/music.mp3