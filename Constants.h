//
// Created by dragoon on 11/13/17.
//

#ifndef CPP_CGDK_CONSTANTS_H
#define CPP_CGDK_CONSTANTS_H

struct Constants {

    static const int influenceMapSize = 64;
    static const int directionsCount = 45;
    static const int fieldsCount = 5;
    static const int closeRangeSpread = 9 * 2 + 1;
    static const int closestRangeSpread = 9;

    static constexpr double PI = 3.14159265358979323846;
    static constexpr double PI_4 = 0.78539816339744830962;
};


#endif //CPP_CGDK_CONSTANTS_H
