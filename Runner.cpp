#include "Runner.h"

#ifdef WITH_VISUALIZER

#include <gui/MainWindow.h>
#include <QtWidgets/QApplication>
#include <gui/WorkingThread.h>

#endif

#include "MyStrategy.h"
#include "Starter.h"

using namespace std;

int main(int argc, char *argv[]) {
#ifdef WITH_VISUALIZER
    QApplication *a = new QApplication(argc, argv);
    MainWindow *w = new MainWindow();
    w->show();
    WorkingThread wt(0, argc, argv);
    wt.start();
    return a->exec();
#else
    return Starter::runProc(argc, argv);
#endif
}

Runner::Runner(const char *host, const char *port, const char *token)
        : remoteProcessClient(host, atoi(port)), token(token) {
}

void Runner::run() {
    if (!remoteProcessClient.inited) {
        return;
    }
    remoteProcessClient.writeTokenMessage(token);
    remoteProcessClient.writeProtocolVersionMessage();
    remoteProcessClient.readTeamSizeMessage();
    model::Game game = remoteProcessClient.readGameContextMessage();

    unique_ptr<MyStrategy> strategy(new MyStrategy);

    shared_ptr<model::PlayerContext> playerContext;
#ifdef WITH_VISUALIZER
    MainWindow::getInstance()->initConstants(game);

    while ((playerContext = remoteProcessClient.readPlayerContextMessage()) != nullptr) {
        model::Player player = playerContext->getPlayer();

        MainWindow::getInstance()->debugMutex.lock();
        MainWindow::getInstance()->prepareTickData(*playerContext, *strategy);

        model::Move move;
        strategy->move(player, playerContext->getWorld(), game, move);

        MainWindow::getInstance()->updateCalculatedStrategyDrawDataTick(*strategy, move);
        MainWindow::getInstance()->finishTick();
        MainWindow::getInstance()->debugMutex.unlock();
        MainWindow::getInstance()->readyToNextFrame();
        remoteProcessClient.writeMoveMessage(move);
    }
#else

    while ((playerContext = remoteProcessClient.readPlayerContextMessage()) != nullptr) {
        model::Player player = playerContext->getPlayer();

        model::Move move;
        strategy->move(player, playerContext->getWorld(), game, move);

        remoteProcessClient.writeMoveMessage(move);
    }
#endif
}
