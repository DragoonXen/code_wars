//
// Created by dragoon on 16.11.17.
//

#ifndef CPP_CGDK_JETCONTROLACTION_H
#define CPP_CGDK_JETCONTROLACTION_H


#include "Action.h"

class JetControlAction : public Action {

public:
    ActionResult doAction(MyStrategy &myStrategy) override;
};


#endif //CPP_CGDK_JETCONTROLACTION_H
