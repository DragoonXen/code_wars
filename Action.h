//
// Created by dragoon on 11/9/17.
//

#ifndef CPP_CGDK_ACTION_H
#define CPP_CGDK_ACTION_H

#include <memory>
#include "VarStorage.h"
#include "ActionResult.h"

class MyStrategy;

struct Action {

private:
    static int idCounter;
public:
    int id;
    bool waitCooldown;
    bool singleSignal;
private:
    std::vector<std::shared_ptr<Action>> signalTo;
    std::vector<Action *> signalFrom;

public:
    Action();

    Action(bool waitCooldown);

    Action(bool waitCooldown, bool singleSignal);

    void sendSignalTo(std::shared_ptr<Action> other) {
        other->signalFrom.push_back(this);
        this->signalTo.push_back(other);
    }

    ActionResult doSmth(MyStrategy &myStrategy);

    virtual ActionResult doAction(MyStrategy &myStrategy) = 0;
};


#endif //CPP_CGDK_ACTION_H
