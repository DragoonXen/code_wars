//
// Created by dragoon on 11/13/17.
//

#ifndef CPP_CGDK_I_AARVINFLUENCEMAPCALC_H
#define CPP_CGDK_I_AARVINFLUENCEMAPCALC_H

#include <cstring>
#include <chrono>
#include <algorithm>
#include "Action.h"
#include "MyStrategy.h"
#include "CalcUtil.h"
#include "SimpleLog.h"

struct I_AARVInfluenceMapCalc : public Action {

    I_AARVInfluenceMapCalc() : Action(false) {}

    ActionResult doAction(MyStrategy &myStrategy) override {
        if (VarStorage::currentWorld->getTickIndex() % 5) {
            return SKIP;
        }

        auto start = std::chrono::steady_clock::now();
        double divizion = CalcUtil::game.getWorldWidth() / Constants::influenceMapSize;
        double (&ARRVInfluenceField)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::ARRVInfluenceField;

        double (&tempLayer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[0];
        double (&layer1)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[1];
        double (&layer2)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[2];
        double (&layer3)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[3];
        double (&layer4)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[4];
        double (&layer5)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[5];
        double (&layer6)[Constants::influenceMapSize][Constants::influenceMapSize] = VarStorage::influenceFieldTemp[6];

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer1);
        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::IFV)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer2);

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::IFV)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer3);
        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::TANK)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer4);

        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::enemyVehiclesByType[int(model::VehicleType::TANK)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer5);
        memset(tempLayer1, 0, sizeof(tempLayer1[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (auto &unit : VarStorage::myVehiclesByType[int(model::VehicleType::HELICOPTER)]) {
            tempLayer1[int(unit->position.x / divizion)][int(unit->position.y / divizion)] += 1.;
        }
        CalcUtil::semiLinearSpreadField(tempLayer1, layer6);


        memset(ARRVInfluenceField, 0, sizeof(ARRVInfluenceField[0][0]) * Constants::influenceMapSize * Constants::influenceMapSize);
        for (int i = 0; i != Constants::influenceMapSize; ++i) {
            for (int j = 0; j != Constants::influenceMapSize; ++j) {

                ARRVInfluenceField[i][j] = std::min(std::min(layer2[i][j] / layer1[i][j],
                                                             std::min(layer4[i][j] / layer3[i][j],
                                                                      layer6[i][j] / layer5[i][j])), 1.5);
                ARRVInfluenceField[i][j] = std::max(ARRVInfluenceField[i][j], 1. / 1.5);
            }
        }


        auto now = std::chrono::steady_clock::now();
        long times = std::chrono::duration_cast<std::chrono::microseconds>(now - start).count();
        LOG_DEBUG("% microseconds", times);
        return SKIP;
    }
};


#endif //CPP_CGDK_I_AARVINFLUENCEMAPCALC_H
