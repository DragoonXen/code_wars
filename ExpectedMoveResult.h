//
// Created by dragoon on 11/18/17.
//

#ifndef CPP_CGDK_EXPECTEDMOVERESULT_H
#define CPP_CGDK_EXPECTEDMOVERESULT_H

struct ExpectedMoveResult {
    int ticksCount;
    int expectedOutcoming;
    int expectedEnemyDeaths;
    int expectedIncoming;
    int expectedMyDeaths;

    ExpectedMoveResult() {
        this->expectedIncoming = 1000000;
        this->expectedMyDeaths = 1000;
        this->expectedOutcoming = 0;
        this->expectedEnemyDeaths = 0;
        this->ticksCount = 0;
    }

    ExpectedMoveResult(int ticksCount, int expectedOutcoming, int expectedEnemyDeaths, int expectedIncoming, int expectedMyDeaths)
            : ticksCount(ticksCount), expectedOutcoming(expectedOutcoming), expectedEnemyDeaths(expectedEnemyDeaths), expectedIncoming(expectedIncoming),
              expectedMyDeaths(expectedMyDeaths) {}

    inline bool operator>(const ExpectedMoveResult &other) {
        if (expectedMyDeaths - expectedEnemyDeaths == other.expectedMyDeaths - other.expectedEnemyDeaths) {
            return expectedOutcoming - expectedIncoming > other.expectedOutcoming - other.expectedIncoming;
        } else {
            return expectedEnemyDeaths - expectedMyDeaths > other.expectedEnemyDeaths - other.expectedMyDeaths;
        }
    }

    inline bool operator<(const ExpectedMoveResult &other) {
        if (expectedMyDeaths - expectedEnemyDeaths == other.expectedMyDeaths - other.expectedEnemyDeaths) {
            return expectedOutcoming - expectedIncoming < other.expectedOutcoming - other.expectedIncoming;
        } else {
            return expectedEnemyDeaths - expectedMyDeaths < other.expectedEnemyDeaths - other.expectedMyDeaths;
        }
    }
};

#endif //CPP_CGDK_EXPECTEDMOVERESULT_H
